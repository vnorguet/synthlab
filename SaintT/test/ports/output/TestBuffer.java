package ports.output;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ports.ISignal;
import ports.Signal;

public class TestBuffer {

	private IBuffer buffer;
	
	@Before
	public void setUp() throws Exception {
		buffer = new Buffer(10);
	}

	@After
	public void tearDown() throws Exception {
		buffer = null;
	}

	@Test
	public void testBuffer() {
		IBuffer b = new Buffer(10);
		
		assertNotNull(b);
		assertNotSame(buffer,b);
		assertEquals(0, b.getSize());
	}
	
	@Test
	public void testGetSize(){
		assertEquals(0,buffer.getSize());
		for(int i=0; i < 10; i++){
			buffer.addNewSignal(new Signal());
			assertEquals(i+1,buffer.getSize());
		}
		assertEquals(10,buffer.getSize());

		// Try to bypass the maximum capacity of the buffer, should not work
		buffer.addNewSignal(new Signal());
		assertEquals(10,buffer.getSize());
	}
	
	@Test
	public void testGetNextValues(){
		assertNull(buffer.getNextSignal());
		
		ISignal s1 = new Signal();
		assertNotNull(s1);
		
		buffer.addNewSignal(s1);
		assertSame(s1, buffer.getNextSignal());
		
		ISignal s2 = new Signal();
		buffer.addNewSignal(s2);
		ISignal s = buffer.getNextSignal();
		assertNotSame(s1,s);
		assertSame(s2,s);
	}
	
	@Test
	public void testAddNewValues(){
		assertNull(buffer.getNextSignal());
		
		ISignal s1 = new Signal();
		assertNotNull(s1);
		
		buffer.addNewSignal(s1);
		assertSame(s1, buffer.getNextSignal());
		
		ISignal s2 = new Signal();
		buffer.addNewSignal(s2);
		ISignal s = buffer.getNextSignal();
		assertNotSame(s1,s);
		assertSame(s2,s);
	}

}

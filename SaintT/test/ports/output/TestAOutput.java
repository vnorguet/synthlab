package ports.output;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ports.ISignal;
import ports.Signal;

public class TestAOutput {

	private IAOutput output;
	
	@Before
	public void setUp() throws Exception {
		output = new AOutput(null, 1);
	}

	@After
	public void tearDown() throws Exception {
		output = null;
	}

	@Test
	public void testAOutput() {
		IAOutput o = new AOutput(null, 1);
		
		assertNotNull(o);
		assertNotSame(output,o);
		assertNull(o.getSignal());
	}

	@Test
	public void testGetSignal() {
		assertNull(output.getSignal());
		
		ISignal s1 = new Signal();
		assertNotNull(s1);
		
		output.addSignal(s1);
		assertSame(s1, output.getSignal());
		
		ISignal s2 = new Signal();
		output.addSignal(s2);
		ISignal s = output.getSignal();
		assertNotSame(s1,s);
		assertSame(s2,s);
	}

	@Test
	public void testAddSignal() {
		assertNull(output.getSignal());
		
		ISignal s1 = new Signal();
		assertNotNull(s1);
		
		output.addSignal(s1);
		assertSame(s1, output.getSignal());
		
		ISignal s2 = new Signal();
		output.addSignal(s2);
		ISignal s = output.getSignal();
		assertNotSame(s1,s);
		assertSame(s2,s);
	}

}

package ports.input;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ports.ISignal;
import ports.Signal;
import ports.output.AOutput;
import ports.output.IAOutput;

public class TestAInput {

	private IAInput input;
	
	@Before
	public void setUp() throws Exception {
		input = new AInput(null);
	}

	@After
	public void tearDown() throws Exception {
		input = null;
	}

	@Test
	public void testAInput() {
		IAInput i = new AInput(null);
		
		assertNotNull(i);
		assertNotSame(input,i);
		assertNull(i.getSource());
	}

	@Test
	public void testPlug() {
		IAOutput source = new AOutput(null, 1);
		assertTrue(input.plug(source));
			
		source = new AOutput(null, 1);
		assertFalse(input.plug(source));
	}

	@Test
	public void testUnplug() {
		IAOutput source = new AOutput(null, 1);
		input.plug(source);
		assertTrue(input.unplug());
		assertFalse(input.unplug());
	}

	@Test
	public void testGetSource() {
		assertNull(input.getSource());
		
		IAOutput source = new AOutput(null, 1);
		input.plug(source);
		assertSame(source, input.getSource());
		
		source = new AOutput(null, 1);
		input.plug(source);
		assertNotSame(source, input.getSource());
		
		input.unplug();
		assertNull(input.getSource());
	}

	@Test
	public void testGetSignal() {
		assertNull(input.getSignal());
		
		IAOutput source = new AOutput(null, 1);
		input.plug(source);
		assertNull(input.getSignal());
		
		ISignal signal = new Signal();
		source.addSignal(signal);
		assertSame(signal,input.getSignal());
	}

}

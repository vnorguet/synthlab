package ports.input;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ports.output.COutput;
import ports.output.ICOutput;

public class TestCInput {
	private ICInput input;

	@Before
	public void setUp() throws Exception {
		input = new CInput("in");
	}

	@After
	public void tearDown() throws Exception {
		input = null;
	}

	@Test
	public void testCInput() {
		ICInput i = new CInput("in");

		assertNotNull(i);
		assertNotSame(input, i);
		assertNotNull(i.getPresentation());
	}

	@Test
	public void testPlug() {
		assertNull(input.getAbstraction().getSource());
		ICOutput output = new COutput(10, "out");
		input.plug(output);
		assertSame(output.getAbstraction(), input.getAbstraction().getSource());
	}

	@Test
	public void testUnplug() {
		assertNull(input.getAbstraction().getSource());
		ICOutput output = new COutput(10, "out");
		input.plug(output);
		assertSame(output.getAbstraction(), input.getAbstraction().getSource());
		input.unplug();
		assertNull(input.getAbstraction().getSource());
	}

	@Test
	public void testGetPresentation() {
		assertNotNull(input.getPresentation());
	}

	@Test
	public void testGetAbstraction() {
		assertNotNull(input.getAbstraction());
	}

}

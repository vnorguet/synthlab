package modifier;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ASwitchTest {
	
	private IASwitch mySwitch;
	private ICSwitch control;

	@Before
	public void setUp() throws Exception {
		
		List <String> listValues = new ArrayList <String>();
		
		listValues.add("sinusoid");
		listValues.add("triangle");
		listValues.add("square");
		listValues.add("sawTeeth");
		mySwitch = new ASwitch(control, listValues, "sinusoid", "Switch");
	}

	@After
	public void tearDown() throws Exception {
		mySwitch = null;
	}

	@Test
	public void testASwitch() {
		List <String> listValues2 = new ArrayList <String>();
		
		listValues2.add("high");
		listValues2.add("low");
		listValues2.add("band");
		listValues2.add("notch");
		IASwitch s = new ASwitch(control, listValues2, "sinusoid", "Switch");
		assertNotNull(s);
		assertNotSame(mySwitch, s);
	}

	@Test
	public void testAddValue() {
		assertEquals("Add OK ?? ", 4, mySwitch.getListValues().size());
		mySwitch.addValue("Test");
		assertEquals("Add OK ?? ", 5, mySwitch.getListValues().size());
		assertEquals("Add OK ?? ", "Test", mySwitch.getListValues().get(4));
	}

	@Test
	public void testGetValue() {
		assertNotNull(mySwitch.getValue());
	}

	@Test
	public void testSetValue() {
		mySwitch.setValue("notch");
		assertNotNull(mySwitch.getValue());
	}

	@Test
	public void testGetName() {
		assertNotNull(mySwitch.getName());
		assertEquals("Init OK ?? ", "Switch", mySwitch.getName());
	}

	@Test
	public void testSetName() {
		mySwitch.setName("Test");
		assertNotNull(mySwitch.getName());
		assertEquals("Name OK ?? ", "Test", mySwitch.getName());
	}

}
package modifier;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class APotentiometerTest {

	private IAPotentiometer potentiometer;
	private ICPotentiometer control;

	@Before
	public void setUp() throws Exception {
		potentiometer = new APotentiometer(control, -7, 7, (short) 0,"Popo");
	}

	@After
	public void tearDown() throws Exception {
		potentiometer = null;
	}
	
	@Test
	public void testAPotentiometer() {
		IAPotentiometer p = new APotentiometer(control, -7, 7, (short) 0,"Popo");
		assertNotNull(p);
		assertNotSame(potentiometer, p);
	}

	@Test
	public void testGetMin() {
		assertNotNull(potentiometer.getMin());
		assertEquals("Min OK ?? ", -7, potentiometer.getMin());
	}

	@Test
	public void testSetMin() {
		potentiometer.setMin(2);
		assertNotNull(potentiometer.getMin());
		assertEquals("Min OK ?? ", 2, potentiometer.getMin());
	}

	@Test
	public void testGetMax() {
		assertNotNull(potentiometer.getMax());
		assertEquals("Max OK ?? ", 7, potentiometer.getMax());
	}

	@Test
	public void testSetMax() {
		potentiometer.setMax(5);
		assertNotNull(potentiometer.getMax());
		assertEquals("Max OK ?? ", 5, potentiometer.getMax());
	}

	@Test
	public void testGetValue() {
		assertNotNull(potentiometer.getValue());
	}

	@Test
	public void testSetValue() {
		potentiometer.setValue((short) 2);
		assertNotNull(potentiometer.getValue());
	}

	@Test
	public void testGetInit() {
		assertNotNull(potentiometer.getInit());
		assertEquals("Init OK ?? ", 0, potentiometer.getInit());
	}

	@Test
	public void testSetInit() {
		potentiometer.setInit(3);
		assertNotNull(potentiometer.getInit());
		assertEquals("Init OK ?? ", 3, potentiometer.getInit());
	}

	@Test
	public void testGetName() {
		assertNotNull(potentiometer.getName());
		assertEquals("Init OK ?? ", "Popo", potentiometer.getName());
	}

	@Test
	public void testSetName() {
		potentiometer.setName("Test");
		assertNotNull(potentiometer.getName());
		assertEquals("Name OK ?? ", "Test", potentiometer.getName());
	}
}
package modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CPotentiometerTest {
	
	private ICPotentiometer myPotentiometer;

	@Before
	public void setUp() throws Exception {
		myPotentiometer = new CPotentiometer(0, 5, (short) 2, "test");
	}

	@After
	public void tearDown() throws Exception {
		myPotentiometer = null;
	}

	@Test
	public void testCPotentiometer() {
		ICPotentiometer p = new CPotentiometer(1, 5, (short) 3, "test");
		assertNotNull(p);
		assertNotSame(myPotentiometer, p);
	}
	
	@Test
	public void testGetPresentation() {
		assertNotNull(myPotentiometer.getPresentation());
	}

	@Test
	public void testGetAbstraction() {
		assertNotNull(myPotentiometer.getAbstraction());
	}

	@Test
	public void testGetValue() {
		assertNotNull(myPotentiometer.getValue());
		assertEquals("Get value ok ", 2, (int)myPotentiometer.getValue());
		assertEquals("Get value ok ", (int)myPotentiometer.getAbstraction().getValue(), (int)myPotentiometer.getValue());
	}

	@Test
	public void testSetValue() {
		assertEquals("Get value ok ", 2, (int)myPotentiometer.getValue());
		myPotentiometer.setValue((short) 3);
		assertNotNull(myPotentiometer.getValue());
		assertEquals("Get value ok ", 3, (int)myPotentiometer.getValue());
	}

	@Test
	public void testNotifyString() {
		fail("Not yet implemented");
	}

}

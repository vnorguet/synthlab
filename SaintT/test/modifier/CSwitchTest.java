package modifier;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CSwitchTest {

	private ICSwitch mySwitch;
	
	@Before
	public void setUp() throws Exception {
		List <String> listValues = new ArrayList <String>();
		
		listValues.add("sinusoid");
		listValues.add("triangle");
		listValues.add("square");
		listValues.add("sawTeeth");
		mySwitch = new CSwitch(listValues, "sinusoid", "Switch");
	}

	@After
	public void tearDown() throws Exception {
		mySwitch = null;
	}

	@Test
	public void testCSwitch() {
		
		List <String> listValues = new ArrayList <String>();
		
		listValues.add("1");
		listValues.add("2");
		listValues.add("3");
		listValues.add("4");
		ICSwitch s = new CSwitch(listValues, "2", "s");
		
		assertNotNull(s);
		assertNotSame(mySwitch, s);
		
	}

	@Test
	public void testGetPresentation() {
		assertNotNull(mySwitch.getPresentation());
	}

	@Test
	public void testGetAbstraction() {
		assertNotNull(mySwitch.getAbstraction());
	}

	@Test
	public void testGetValue() {
		assertNotNull(mySwitch.getValue());
		assertEquals("Get value ok ", mySwitch.getAbstraction().getValue(), mySwitch.getValue());
	}

	@Test
	public void testSetValue() {
		mySwitch.setValue("2");
		assertNotNull(mySwitch.getValue());
		assertEquals("Set value ok ", "2", mySwitch.getValue());
	}

	@Test
	public void testNotifyString() {
		fail("Not yet implemented");
	}

}

package modules.replicator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ports.output.AOutput;
import ports.output.COutput;
import ports.output.IAOutput;
import ports.output.ICOutput;

public class AReplicatorTest {

	private IAReplicator abstraction;
	private ICOutput controller;
	private IAOutput out;
	
	@Before
	public void setUp() throws Exception {
		abstraction = new AReplicator();
		
		controller = new COutput(5, "out");
		
		 out = new AOutput(controller,9);
	}

	@After
	public void tearDown() throws Exception {
		abstraction = null;
		controller=null;
		out=null;
	}
	
	@Test
	public void setOut1test() {
		
		abstraction.setOut1(out);
			
		assertEquals(out, abstraction.getOut1());
		assertNotNull(abstraction.getOut1());
		
	
	}
	
	@Test
	public void setOut2test() {
		
		abstraction.setOut2(out);
			
		assertEquals(out, abstraction.getOut2());
		assertNotNull(abstraction.getOut2());
		assertNotSame(out, abstraction.getOut1());
		
	
	}
	
	@Test
	public void setOut3test() {
		
		abstraction.setOut3(out);
			
		assertEquals(out, abstraction.getOut3());
		assertNotNull(abstraction.getOut3());
		assertSame(out, abstraction.getOut3());
		assertNotSame(out, abstraction.getOut2());
		
	
	}
	
	@Test
	public void getOut1test() {
		
		
		abstraction.setOut1(out);
			
		assertEquals(out, abstraction.getOut1());
		assertNotNull(abstraction.getOut1());
		
	
	}
	
	
}

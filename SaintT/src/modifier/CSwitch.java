package modifier;

import java.util.ArrayList;
import java.util.List;

import modules.generic.ICModule;

/**
 * Control class that implements essential features of a switch presentation in
 * order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */

public class CSwitch implements ICSwitch {

	/**
	 * attribute used to access the attached presentation.
	 */
	private IPSwitch presentation;

	/**
	 * Attribute used to access the attached abstraction.
	 */
	private IASwitch abstraction;

	/**
	 * Attribute used to access the list of modules.
	 */
	private List<ICModule> listModules = new ArrayList<ICModule>();

	/**
	 * Build an control component of the switch in order to be graphically
	 * visualized.
	 * 
	 * @param listValues
	 *            list of values of the switch
	 * @param name
	 *            name of the switch
	 */
	public CSwitch(List<String> listValues, String init, String name) {
		abstraction = new ASwitch(this, listValues, init, name);
		presentation = new PSwitch(listValues, name, init, this);
	}

	/**
	 * @see modifier.ICSwitch#getPresentation()
	 */
	@Override
	public IPSwitch getPresentation() {
		return presentation;
	}

	/**
	 * @see modifier.ICSwitch#getAbstraction()
	 */
	@Override
	public IASwitch getAbstraction() {
		return abstraction;
	}

	/**
	 * @see modifier.ICSwitch#getValue()
	 */
	@Override
	public String getValue() {
		return abstraction.getValue();
	}

	/**
	 * @see modifier.ICSwitch#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String v) {
		abstraction.setValue(v);
	}

	/**
	 * @see modifier.ICSwitch#addModuleListener(modules.generic.ICModule)
	 */
	@Override
	public void addModuleListener(ICModule module) {
		listModules.add(module);
	}

	/**
	 * @see modifier.ICSwitch#notifyModules()
	 */
	@Override
	public void notifyModules() {
		for (ICModule m : listModules) {
			System.out.println(">>> ");
			m.notify(abstraction.getName());
		}
	}
}
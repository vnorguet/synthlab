package modifier;

import java.util.List;

/**
 * Interface of the abstraction class that describes essential features of an
 * switch.
 * 
 * @author Les Z'Octaves
 */

public interface IASwitch {

	/**
	 * Sets the current value of the switch.
	 * 
	 * @param value
	 *            the new current value of the switch to associate
	 */
	public void setValue(String value);

	/**
	 * Returns the current value of the switch.
	 * 
	 * @return the current value of the switch
	 */
	public String getValue();

	/**
	 * Add a value in the list of values.
	 * 
	 * @param value
	 *            the value to add.
	 */
	public void addValue(String value);

	/**
	 * Returns the name of the switch.
	 * 
	 * @return the name of the switch
	 */
	public String getName();

	/**
	 * Sets the name of the switch.
	 * 
	 * @param name
	 *            the new name of the switch to associate
	 */
	public void setName(String name);

	/**
	 * Returns the list of values of the switch.
	 * 
	 * @return the list of values of the switch
	 */
	public List<String> getListValues();

	/**
	 * Sets the the list of values of the switch.
	 * 
	 * @param listValues
	 *            the new list of values of the switch to associate
	 */
	public void setListValues(List<String> listValues);
}
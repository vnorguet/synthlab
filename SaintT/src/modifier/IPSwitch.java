package modifier;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.RenderingHints;

/**
 * Interface of the presentation that describes essential features of a switch
 * presentation for a graphic illustration.
 * 
 * @author Les Z'Octaves
 */

public interface IPSwitch {

	public final static float START = 225;
	public final static float LENGTH = 270;
	public final static float PI = 3.1415f;
	public final static float START_ANG = (START / 360) * PI * 2;
	public final static float LENGTH_ANG = (LENGTH / 360) * PI * 2;
	public final static float MULTIP = 180 / PI;
	public final static Color DEFAULT_FOCUS_COLOR = new Color(0x8080ff);

	// Dimensions
	public final static Dimension MIN_SIZE = new Dimension(80, 80);
	public final static Dimension PREF_SIZE = new Dimension(100, 100);

	// Set the antialiasing to get the right look!
	public final static RenderingHints AALIAS = new RenderingHints(
			RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	// Drag type
	public final static int SIMPLE = 1;
	public final static int ROUND = 2;

	// Shadow
	public final static int SHADOWX = 1;
	public final static int SHADOWY = 1;

	// Drag
	public final static float DRAG_SPEED = 0.01f;
	public final static float CLICK_SPEED = 0.01f;

	/**
	 * Returns the value of the switch.
	 * 
	 * @return the value of the switch.
	 */
	public float getValue();

	/**
	 * Sets the new value of the switch.
	 * 
	 * @param val
	 *            the new value of the switch to associate.
	 */
	public void setValue(float val);

	/**
	 * Paint the potentiometer
	 * 
	 * @param g
	 *            Graphic
	 */
	public void paint(Graphics g);

	/**
	 * Returns the control component of the switch.
	 * 
	 * @return the control component of the switch.
	 */
	public ICSwitch getControl();

	/**
	 * Sets the control of the switch.
	 * 
	 * @param control
	 *            the new control of the switch to associate
	 */
	public void setControl(ICSwitch control);

}
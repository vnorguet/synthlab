package modifier;

/**
 * Interface of the abstraction class that describes essential features of an
 * potentiometer.
 * 
 * @author Les Z'Octaves
 */

public interface IAPotentiometer {

	/**
	 * Returns the minimum value of the potentiometer.
	 * 
	 * @return the minimum value of the potentiometer
	 */
	public int getMin();

	/**
	 * Sets the minimum value of the potentiometer.
	 * 
	 * @param min
	 *            the new minimum value of the potentiometer to associate
	 */
	public void setMin(int min);

	/**
	 * Returns the maximum value of the potentiometer.
	 * 
	 * @return the maximum value of the potentiometer
	 */
	public int getMax();

	/**
	 * Sets the maximum value of the potentiometer.
	 * 
	 * @param max
	 *            the new maximum value of the potentiometer to associate
	 */
	public void setMax(int max);

	/**
	 * Returns the current value of the potentiometer.
	 * 
	 * @return the current value of the potentiometer
	 */
	public short getValue();

	/**
	 * Sets the current value of the potentiometer.
	 * 
	 * @param value
	 *            the new current value of the potentiometer to associate
	 */
	public void setValue(short f);

	/**
	 * Returns the initial value of the potentiometer.
	 * 
	 * @return the initial value of the potentiometer
	 */
	public int getInit();

	/**
	 * Sets the initial value of the potentiometer.
	 * 
	 * @param init
	 *            the new initial value of the potentiometer to associate
	 */
	public void setInit(int init);

	/**
	 * Returns the name of the potentiometer.
	 * 
	 * @return the name of the potentiometer
	 */
	public String getName();

	/**
	 * Sets the name of the potentiometer.
	 * 
	 * @param name
	 *            the new name of the potentiometer to associate
	 */
	public void setName(String name);
}
package modifier;

import java.util.List;

/**
 * Abstraction class that implements the interface IASwitch and provides
 * essential features of a switch.
 * 
 * @author Les Z'Octaves
 */

public class ASwitch implements IASwitch {

	/**
	 * Current value of the switch.
	 */
	private String value;

	/**
	 * List of values of the switch.
	 */
	private List<String> listValues;

	/**
	 * Name of the switch.
	 */
	private String name;

	/**
	 * 
	 */
	private ICSwitch control;

	private String init;

	/**
	 * Build an abstraction component of the switch.
	 * 
	 * @param listValues
	 *            list of values of the switch.
	 * @param name
	 *            name of the switch.
	 */
	public ASwitch(ICSwitch c, List<String> listValues, String init, String name) {
		this.listValues = listValues;
		this.name = name;
		this.value = listValues.get(0);
		this.control = c;
		this.init = init;
	}

	/**
	 * @see modifier.IASwitch#addValue(java.lang.String)
	 */
	@Override
	public void addValue(String value) {
		listValues.add(value);
	}

	/**
	 * @see modifier.IASwitch#getValue()
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * @see modifier.IASwitch#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
		control.notifyModules();
	}

	/**
	 * @see modifier.IASwitch#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see modifier.IASwitch#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see modifier.IASwitch#getListValues()
	 */
	@Override
	public List<String> getListValues() {
		return listValues;
	}

	/**
	 * @see modifier.IASwitch#setListValues(java.util.List)
	 */
	@Override
	public void setListValues(List<String> listValues) {
		this.listValues = listValues;
	}

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}
}
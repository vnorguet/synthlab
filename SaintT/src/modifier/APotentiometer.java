package modifier;

/**
 * Abstraction class that implements the interface IAPotentiometer and provides
 * essential features of a potentiometer.
 * 
 * @author Les Z'Octaves
 */

public class APotentiometer implements IAPotentiometer {

	/**
	 * Minimum value of the potentiometer
	 */
	private int min;

	/**
	 * Maximum value of the potentiometer
	 */
	private int max;

	/**
	 * Current value of the potentiometer
	 */
	private short value;

	/**
	 * Init value of the potentiometer
	 */
	private int init;

	/**
	 * Init value of the potentiometer
	 */
	private String name;

	/**
	 * Attribute used to access the attached control.
	 */
	private ICPotentiometer control;

	/**
	 * Build an abstraction component of the potentiometer.
	 * 
	 * @param control
	 *            the attached control component
	 * @param minV
	 *            the minimum value of the potentiometer
	 * @param maxV
	 *            the maximum value of the potentiometer
	 * @param initV
	 *            the initial value of the potentiometer
	 * @param nameV
	 *            the name of the potentiometer
	 */
	public APotentiometer(ICPotentiometer control, int minV, int maxV,
			short initV, String nameV) {
		this.min = minV;
		this.max = maxV;
		this.init = initV;
		this.value = initV;
		this.name = nameV;
		this.control = control;
	}

	/**
	 * @see modifier.IAPotentiometer#getMin()
	 */
	@Override
	public int getMin() {
		return min;
	}

	/**
	 * @see modifier.IAPotentiometer#setMin(int)
	 */
	@Override
	public void setMin(int min) {
		this.min = min;
	}

	/**
	 * @see modifier.IAPotentiometer#getMax()
	 */
	@Override
	public int getMax() {
		return max;
	}

	/**
	 * @see modifier.IAPotentiometer#setMax(int)
	 */
	@Override
	public void setMax(int max) {
		this.max = max;
	}

	/**
	 * @see modifier.IAPotentiometer#getValue()
	 */
	@Override
	public short getValue() {
		return value;
	}

	/**
	 * @see modifier.IAPotentiometer#setValue(short)
	 */
	@Override
	public void setValue(short value) {
		this.value = value;
		control.notifyModules();
	}

	/**
	 * @see modifier.IAPotentiometer#getInit()
	 */
	@Override
	public int getInit() {
		return init;
	}

	/**
	 * @see modifier.IAPotentiometer#setInit(int)
	 */
	@Override
	public void setInit(int init) {
		this.init = init;
	}

	/**
	 * @see modifier.IAPotentiometer#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see modifier.IAPotentiometer#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
}
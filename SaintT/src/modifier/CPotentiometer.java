package modifier;

import java.util.ArrayList;
import java.util.List;

import modules.generic.ICModule;

/**
 * Control class that implements essential features of a potentiometer
 * presentation in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */

public class CPotentiometer implements ICPotentiometer {

	/**
	 * attribute used to access the attached presentation.
	 */
	private IPPotentiometer presentation;

	/**
	 * Attribute used to access the attached abstraction.
	 */
	private IAPotentiometer abstraction;

	/**
	 * Attribute used to access the list of modules.
	 */
	private List<ICModule> listModules = new ArrayList<ICModule>();
	
	/**
	 * Build an control component of the potentiometer in order to be
	 * graphically visualized.
	 * 
	 * @param min
	 *            the minimum value of the potentiometer
	 * @param max
	 *            the maximum value of the potentiometer
	 * @param init
	 *            the initial value of the potentiometer
	 * @param name
	 *            the name of the potentiometer
	 */
	public CPotentiometer(int min, int max, short init, String name) {
		presentation = new PPotentiometer(min, max, init, name, this);
		abstraction = new APotentiometer(this, min, max, init, name);
	}

	/**
	 * @see modifier.ICPotentiometer#getPresentation()
	 */
	@Override
	public IPPotentiometer getPresentation() {
		return presentation;
	}

	/**
	 * @see modifier.ICPotentiometer#getAbstraction()
	 */
	@Override
	public IAPotentiometer getAbstraction() {
		return abstraction;
	}

	/**
	 * @see modifier.ICPotentiometer#getValue()
	 */
	@Override
	public short getValue() {
		return abstraction.getValue();
	}

	/**
	 * @see modifier.ICPotentiometer#setValue(short)
	 */
	@Override
	public void setValue(short f) {
		abstraction.setValue(f);
	}

	/**
	 * @see modifier.ICPotentiometer#addModuleListener(modules.generic.ICModule)
	 */
	@Override
	public void addModuleListener(ICModule module) {
		listModules.add(module);
	}

	/**
	 * @see modifier.ICPotentiometer#notifyModules()
	 */
	@Override
	public void notifyModules() {
		for (ICModule m : listModules) {
			m.notify(abstraction.getName());
		}
	}
}
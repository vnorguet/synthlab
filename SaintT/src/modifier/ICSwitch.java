package modifier;

import modules.generic.ICModule;

/**
 * Interface of the control that describes essential features of a switch
 * control for a graphic illustration.
 * <ul>
 * <li>should be able to get back the presentation component associated.
 * </ul>
 * 
 * @author Les Z'Octaves
 */

public interface ICSwitch {

	/**
	 * Returns the presentation component of the switch.
	 * 
	 * @return the presentation component of the switch associate.
	 */
	public IPSwitch getPresentation();

	/**
	 * Returns the abstraction component of the switch.
	 * 
	 * @return the abstraction component of the switch associate.
	 */
	public IASwitch getAbstraction();

	/**
	 * Returns the current value of the switch by delegating to the component
	 * associated.
	 * 
	 * @return the value of the switch.
	 */
	public String getValue();

	/**
	 * Sets the current value of the switch by delegating to the component
	 * associated.
	 * 
	 * @param f
	 *            the new current value of the switch to associate
	 */
	public void setValue(String v);

	/**
	 * Notify a modification of value in the module.
	 */
	public void notifyModules();

	/**
	 * Add a ICModule to the list of modules.
	 * 
	 * @param module
	 *            the ICModule to add.
	 */
	void addModuleListener(ICModule module);
}
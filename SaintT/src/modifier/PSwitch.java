package modifier;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;

/**
 * Presentation class that implements essential features of a switch
 * presentation in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */

public class PSwitch extends JComponent implements IPSwitch {

	private static final long serialVersionUID = 1L;

	private int size;
	private int middle;

	private int dragType = ROUND;

	private ChangeEvent changeEvent = null;
	private EventListenerList listenerList = new EventListenerList();

	private Arc2D hitArc = new Arc2D.Float(Arc2D.PIE);

	private float ang = START_ANG;
	private float val;
	private int dragpos = -1;

	private double lastAng;
	private float startVal;

	private String nameSwitch = "";
	private int sizeList;
	private List<String> listVal;

	/**
	 * attribute used to access the attached control.
	 */
	private ICSwitch control;

	/**
	 * Build an presentation component of the switch.
	 * 
	 * @param listValues the list of values of the switch
	 * @param name the name of the switch
	 * @param cp the control component associate
	 */
	public PSwitch(java.util.List<String> listValues, String name, String init, ICSwitch cp) {

		// init !!!
		nameSwitch = name;
		sizeList = listValues.size() - 1;
		listVal = listValues;

		//DEGUIIII : a changer
		if (init == "square"){
			setValue(0.33f);
		}
		
		setPreferredSize(PREF_SIZE);
		hitArc.setAngleStart(235); // Degrees ??? Radians???

		// Control du composant
		control = cp;
		control.setValue(listVal.get(0));

		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				dragpos = me.getX() + me.getY();
				startVal = val;

				// Fix last angle
				int xpos = middle - me.getX();
				int ypos = middle - me.getY();
				lastAng = Math.atan2(xpos, ypos);

				requestFocus();
			}

			public void mouseClicked(MouseEvent me) {
				hitArc.setAngleExtent(-(LENGTH + 20));
				if (hitArc.contains(me.getX(), me.getY())) {
					hitArc.setAngleExtent(MULTIP * (ang - START_ANG) - 10);
					if (hitArc.contains(me.getX(), me.getY())) {
						decValue();

					} else
						incValue();
				}
			}

			// Set the value on mouseReleased
			// sets the control of the switch
			public void mouseReleased(MouseEvent e) {
				val = getValue();

				// ///////// A REFACTORER //////////////
				if (val > 0 && val < (1 / (sizeList * 2.f))) {
					setValue(0);
					control.setValue(listVal.get(0));
				}

				if (val > (1 / 6.f) && val < (3 / 6.f)) {
					setValue(0.33f);
					control.setValue(listVal.get(1));
				}

				if (val > (3 / 6.f) && val < (5 / 6.f)) {
					setValue(0.66f);
					control.setValue(listVal.get(2));
				}

				if (val > (5 / 6.f) && val < (7 / 6.f)) {
					setValue(1);
					control.setValue(listVal.get(3));
				}

				/*
				 * int i = 1; //System.out.println(sizeList); for (int cpt = 1;
				 * cpt < sizeList*2; cpt++){ System.out.println("test : "+cpt);
				 * if (val > (cpt/(sizeList*2.f)) && val <
				 * (cpt+2/(sizeList*2.f))){ setValue((float)i/sizeList); }
				 * cpt++; i++; }
				 */
			}

		});

		// Let the user control the knob with the mouse
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent me) {
				if (dragType == SIMPLE) {
					float f = DRAG_SPEED
							* (float) ((me.getX() + me.getY()) - dragpos);
					setValue(startVal + f);
				} else if (dragType == ROUND) {
					// Measure relative the middle of the button!

					int xpos = middle - me.getX();
					int ypos = middle - me.getY();
					double ang = Math.atan2(xpos, ypos);
					double diff = lastAng - ang;

					setValue((float) (getValue() + (diff / LENGTH_ANG)));
					lastAng = ang;
				}
			}

			public void mouseMoved(MouseEvent me) {
			}
		});

		// Let the user control the knob with the keyboard
		addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent e) {
			}

			public void keyReleased(KeyEvent e) {
			}

			public void keyPressed(KeyEvent e) {
				int k = e.getKeyCode();
				if (k == KeyEvent.VK_RIGHT)
					incValue();
				else if (k == KeyEvent.VK_LEFT)
					decValue();
			}
		});

		// Handle focus so that the knob gets the correct focus highlighting.
		addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				repaint();
				getParent().validate();
			}

			public void focusLost(FocusEvent e) {
				repaint();
				getParent().validate();
			}
		});
	}

	/**
	 * Increase the value of the switch.
	 */
	private void incValue() {
		setValue(val + CLICK_SPEED);

	}

	/**
	 * Decrease the value of the switch.
	 */
	private void decValue() {
		setValue(val - CLICK_SPEED);

	}

	/**
	 * @see modifier.IPSwitch#getValue()
	 */
	@Override
	public float getValue() {
		return val;
	}

	/**
	 * @see modifier.IPSwitch#setValue(float)
	 */
	@Override
	public void setValue(float val) {

		if (val < 0) {
			val = 0;
		}

		if (val > 1) {
			val = 1;
		}

		this.val = val;
		ang = START_ANG - (float) LENGTH_ANG * val;

		repaint();
		fireChangeEvent();
		if (getParent() != null) {
			getParent().validate();
		}
	}

	/**
	 * fireChangeEvent
	 */
	protected void fireChangeEvent() {
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == ChangeListener.class) {
				// Lazily create the event:
				if (changeEvent == null)
					changeEvent = new ChangeEvent(this);
				((ChangeListener) listeners[i + 1]).stateChanged(changeEvent);
			}
		}
	}

	/**
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
		int width = getWidth() - 20;
		int height = getHeight() - 20;

		size = Math.min(width, height) - 22;
		middle = (0 + size / 2) + 20;

		if (g instanceof Graphics2D) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.setBackground(getParent().getBackground());
			g2d.addRenderingHints(AALIAS);

			// For the size of the "mouse click" area
			hitArc.setFrame(4, 4, size + 12, size + 12);
		}

		int cpt = 0;
		// Paint the "markers"
		for (float a2 = START_ANG; a2 >= START_ANG - LENGTH_ANG; a2 = a2
				- (float) (LENGTH_ANG / (sizeList + 0.01))) {
			int x = 30 + size / 2 + (int) ((6 + size / 2) * Math.cos(a2));
			int y = 30 + size / 2 - (int) ((6 + size / 2) * Math.sin(a2));
			g.drawLine(30 + size / 2, 30 + size / 2, x, y);

			// Creation de l'image corespondante
			BufferedImage img = null;
			try {
				img = ImageIO
						.read(new File("res/modifiers/" + listVal.get(cpt) + ".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Affichage image
			if (cpt > sizeList / 2) {
				g.drawImage(img, x + 5, y - 10, null);
			} else {
				g.drawImage(img, x - 20, y - 10, null);
			}
			cpt++;
		}

		// Centrage du title
		int borderX = 40;
		if (nameSwitch.length() > 9) {
			borderX = 30;
		}

		// Libele du btn
		g.drawString(nameSwitch, borderX, 15);

		// Paint focus if in focus
		if (hasFocus()) {
			g.setColor(DEFAULT_FOCUS_COLOR);
		} else {
			g.setColor(Color.white);
		}

		g.fillOval(30, 30, size, size);
		g.setColor(Color.gray);
		g.fillOval(34 + SHADOWX, 34 + SHADOWY, size - 8, size - 8);

		g.setColor(Color.black);
		g.drawArc(30, 30, size, size, 315, 270);
		g.fillOval(34, 34, size - 8, size - 8);
		g.setColor(Color.white);

		int x = 30 + size / 2 + (int) (size / 2 * Math.cos(ang));
		int y = 30 + size / 2 - (int) (size / 2 * Math.sin(ang));
		g.drawLine(30 + size / 2, 30 + size / 2, x, y);
		g.setColor(Color.gray);
		int s2 = (int) Math.max(size / 6, 6);
		g.drawOval(30 + s2, 30 + s2, size - s2 * 2, size - s2 * 2);

		int dx = (int) (2 * Math.sin(ang));
		int dy = (int) (2 * Math.cos(ang));
		g.drawLine(30 + dx + size / 2, 30 + dy + size / 2, x, y);
		g.drawLine(30 - dx + size / 2, 30 - dy + size / 2, x, y);
	}

	/**
	 * @see modifier.IPSwitch#getControl()
	 */
	@Override
	public ICSwitch getControl() {
		return control;
	}

	/**
	 * @see modifier.IPSwitch#setControl(modifier.ICSwitch)
	 */
	@Override
	public void setControl(ICSwitch control) {
		this.control = control;
	}
}
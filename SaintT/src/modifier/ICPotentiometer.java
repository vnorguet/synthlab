package modifier;

import modules.generic.ICModule;

/**
 * Interface of the control that describes essential features of a potentiometer
 * control for a graphic illustration.
 * <ul>
 * <li>should be able to get back the presentation component associated.
 * </ul>
 * 
 * @author Les Z'Octaves
 */

public interface ICPotentiometer {

	/**
	 * Returns the presentation component of the potentiometer.
	 * 
	 * @return the presentation component of the potentiometer associate.
	 */
	public IPPotentiometer getPresentation();

	/**
	 * Returns the abstraction component of the potentiometer.
	 * 
	 * @return the abstraction component of the potentiometer associate.
	 */
	public IAPotentiometer getAbstraction();

	/**
	 * Returns the current value of the potentiometer by delegating to the
	 * component associated.
	 * 
	 * @return the value of the potentiometer.
	 */
	public short getValue();

	/**
	 * Sets the current value of the potentiometer by delegating to the
	 * component associated.
	 * 
	 * @param f
	 *            the new current value of the potentiometer to associate
	 */
	public void setValue(short f);

	/**
	 * Notify a modification of value in the module.
	 */
	void notifyModules();

	/**
	 * Add a ICModule to the list of modules.
	 * 
	 * @param module
	 *            the ICModule to add.
	 */
	void addModuleListener(ICModule module);
}
package ports.input;

import ports.output.ICOutput;

public interface ICInput {
	public boolean plug(ICOutput source);
	public void unplug();
	public IPInput getPresentation();
	public IAInput getAbstraction();
	public ICOutput getSource();
}

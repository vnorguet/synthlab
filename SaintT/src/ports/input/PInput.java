package ports.input;

import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modules.generic.IPModule;

import ports.output.IPOutput;

public class PInput extends JPanel implements IPInput {
	private static final long serialVersionUID = 1L;
	private IPOutput output;
	JLabel port;

	public PInput(String name) {
		super();
		/*
		 * setBackground(Color.BLACK); setSize(20,20);
		 * setMinimumSize(getSize()); setMaximumSize(getSize());
		 * setPreferredSize(getSize());
		 */
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(IPModule.COLOR_CONTAINER);
		port = new JLabel(new ImageIcon("res/ports/port.png"));
		add(port);
		add(new JLabel(name));
	}

	public void plug(IPOutput presentation) {
		output = presentation;
		port.setIcon(new ImageIcon("res/ports/greenPort.png"));
		// setBackground(Color.GREEN);
		output.plug();
	}

	public void unplug() {
		// setBackground(Color.BLACK);
		port.setIcon(new ImageIcon("res/ports/port.png"));
		output.unplug();
		output = null;
	}

	@Override
	public boolean contains(Point point, Point correction) {
		return contains(point.x - correction.x, point.y - correction.y);
	}
}

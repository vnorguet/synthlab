package ports.input;

import ports.ISignal;
import ports.output.IAOutput;

public class AInput implements IAInput {
	private IAOutput source;
	private ICInput controller;

	public AInput(ICInput controller) {
		this.controller = controller;
	}

	public synchronized boolean plug(IAOutput source) {
		boolean success = false;
		if (this.source == null) {
			this.source = source;
			success = true;
		}
		notify();
		return success;
	}

	public synchronized boolean unplug() {
		boolean success = false;
		if (source != null) {
			source = null;
			success = true;
		}
		return success;

	}

	public synchronized IAOutput getSource() {
		if (source == null) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return source;
	}
	
	public boolean isPlugged(){
		return (source != null);
	}

	public ISignal getSignal() {
		return (source == null) ? null : source.getSignal();
	}

	@Override
	public ICInput getController() {
		return controller;
	}
}

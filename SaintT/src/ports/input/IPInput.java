package ports.input;

import java.awt.Point;

import ports.output.IPOutput;

public interface IPInput {

	public void plug(IPOutput presentation);

	public void unplug();

	public boolean contains(Point point, Point correction);
}

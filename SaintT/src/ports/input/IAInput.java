package ports.input;

import ports.ISignal;
import ports.output.IAOutput;

public interface IAInput {
	/**
	 * Plug an output in the input if the slot is empty.
	 * The source will not be plugged if an other is already plugged.
	 * @param source
	 *  the output to link with
	 * @return true if the source was successfully plugged, false if an other source was already pluged
	 */
	public boolean plug(IAOutput source);
	
	/**
	 * Unplugs the previously plugged source.
	 * Do nothing if nothing is plugged.
	 * @return true if a source is unplugged, false if there was no source pluged
	 */
	public boolean unplug();
	
	/**
	 * Access to the source.
	 * @return the source of the input
	 */
	public IAOutput getSource();
	
	/**
	 * Get the signal from the source.
	 * @return the signal from the source
	 */
	public ISignal getSignal();
	public ICInput getController();
	public boolean isPlugged();
}

package ports.input;

import ports.output.ICOutput;

public class CInput implements ICInput {
	private IAInput abstraction;
	private IPInput presentation;
	private ICOutput source;

	public CInput(String name) {
		abstraction = new AInput(this);
		presentation = new PInput(name);
	}

	public boolean plug(ICOutput source) {
		boolean res = false;
		if (abstraction.plug(source.getAbstraction())) {
			this.source = source;
			presentation.plug(source.getPresentation());
			source.setPlugged(true);
			res = true;
		}
		return res;
	}

	public void unplug() {
		if (abstraction.unplug()) {
			source.setPlugged(false);
			source.unplug();
			source = null;
			presentation.unplug();
		}

	}

	public IPInput getPresentation() {
		return presentation;
	}

	public IAInput getAbstraction() {
		return abstraction;
	}

	@Override
	public ICOutput getSource() {
		return source;
	}
}

package ports;

import java.util.ArrayList;
import java.util.List;

public class Signal implements ISignal {
	private List<Short> values;
	
	public static ISignal multiplication(ISignal one, ISignal two){  //TODO: do this on place, don't create a temporary signal.
		ISignal res = new Signal();
		List<Short> oneValues = one.getValues();
		List<Short> twoValues = two.getValues();
		for(int i=0; i<ISignal.CAPACITY; i++){
			res.addValue((short)(oneValues.get(i) * twoValues.get(i)));
		}
		return res;
	}
	
	public Signal(){
		values = new ArrayList<Short>();
	}
	
	public Signal(List<Short> initList){
		values = new ArrayList<Short>();
		addValues(initList);
	}

	public boolean addValue(Short value){
		boolean res = false;
		if(values.size() < CAPACITY){
			values.add(value);
			res = true;
		}
		return res;
	}
	
	public int addValues(List<Short> values){
		int nbAdded = 0;
		for(Short f : values){
			if(addValue(f)){
				nbAdded++;
			}else{
				break;
			}
		}
		return nbAdded;
	}
	
	@Override
	public List<Short> getValues() {
		return new ArrayList<Short>(values);
	}
}

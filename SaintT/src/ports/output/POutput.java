package ports.output;

import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modules.generic.IPModule;

public class POutput extends JPanel implements IPOutput {
	private static final long serialVersionUID = 1L;
	private JLabel port;
	

	public POutput(String name){/*
		setBackground(Color.BLACK);
		setSize(20,20);
		setMinimumSize(getSize());
		setMaximumSize(getSize());
		setPreferredSize(getSize());*/
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(IPModule.COLOR_CONTAINER);
		port = new JLabel(new ImageIcon("res/ports/port.png"));
		add(port);
		add(new JLabel(name));
	}
	
	@Override
	public void plug() {
		//setBackground(Color.RED);
		port.setIcon(new ImageIcon("res/ports/redPort.png"));
	}

	@Override
	public void unplug() {
		//setBackground(Color.BLACK);
		port.setIcon(new ImageIcon("res/ports/port.png"));
	}

	@Override
	public void waitForPlug() {
		//setBackground(Color.ORANGE);
		port.setIcon(new ImageIcon("res/ports/orangePort.png"));
	}

	@Override
	public boolean contains(Point point, Point correction) {
		return contains(point.x-correction.x, point.y-correction.y);
	}
}

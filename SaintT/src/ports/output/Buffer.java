package ports.output;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import ports.ISignal;

public class Buffer implements IBuffer {
	private Queue<ISignal> signalQueue;
	private int size;

	public Buffer(int size) {
		this.size = size;
		signalQueue = new ArrayBlockingQueue<ISignal>(size);
	}

	public int getSize() {
		return signalQueue.size();
	}

	public synchronized ISignal getNextSignal() {
		while (isEmpty()) {
//			System.out.println("buffer empty, waiting");
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		ISignal res = signalQueue.poll();
			notify();
		return res;
	}

	public synchronized void addNewSignal(ISignal signal) {
		while (signalQueue.size() >= this.size) {
			//System.out.println("buffer full, waiting");
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		notify();
		signalQueue.add(signal);
	}

	public boolean isEmpty() {
		return (signalQueue.size() == 0) ? true : false;
	}
}

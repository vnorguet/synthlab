package ports.output;

import modules.generic.ICModule;

public class COutput implements ICOutput {
	private IAOutput abstraction;
	private IPOutput presentation;
	private ICModule module;
	private boolean plugged;
	
	public COutput(int bufferSize, String name) {
		abstraction = new AOutput(this, bufferSize);
		presentation = new POutput(name);
	}
	
	
	public IAOutput getAbstraction() {
		return abstraction;
	}

	public IPOutput getPresentation() {
		return presentation;
	}

	public ICModule getModule() {
		return module;
	}


	@Override
	public void waitForPlug() {
		presentation.waitForPlug();		
	}


	@Override
	public void setPlugged(boolean b) {
		plugged = b;
	}

	@Override
	public boolean isPlugged(){
		return plugged;
	}


	@Override
	public void unplug() {
		presentation.unplug();
	}
}

package ports.output;

import java.awt.Point;

public interface IPOutput {
	public void plug();
	public void unplug();
	public void waitForPlug();
	public boolean contains(Point point, Point correction);
}

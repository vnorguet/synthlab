package ports.output;

import ports.ISignal;

public interface IBuffer {
	
	/**
	 * Give the number of block in the buffer.
	 * @return the number of block of the buffer can stock
	 */
	public int getSize();
	
	/**
	 * Retrieve and remove the oldest block entered in the buffer and return it.
	 * @return the oldest block entered
	 */
	public ISignal getNextSignal();
	
	/**
	 * Add a new block to the buffer.
	 * @param block
	 *  the block to add
	 * @throws InterruptedException 
	 */
	public void addNewSignal(ISignal block);

	public boolean isEmpty();
}

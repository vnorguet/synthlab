package ports.output;

import modules.generic.ICModule;

public interface ICOutput {

	public IAOutput getAbstraction();

	public IPOutput getPresentation();

	public ICModule getModule();

	public void waitForPlug();

	public void setPlugged(boolean b);
	
	public boolean isPlugged();
	
	public void unplug();
}

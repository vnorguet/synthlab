package ports.output;

import ports.ISignal;

public interface IAOutput {	
	/**
	 * Get the signal from the buffer.
	 * @return the signal from the buffer
	 */
	public ISignal getSignal();
	
	/**
	 * Add a signal into the output
	 * @throws InterruptedException 
	 */
	public void addSignal(ISignal signal);
	public ICOutput getController();

}

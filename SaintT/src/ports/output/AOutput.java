package ports.output;

import ports.ISignal;

public class AOutput implements IAOutput {
	private IBuffer buffer;
	private ICOutput controller;

	public AOutput(ICOutput controller, int bufferSize) {
		this.controller = controller;
		this.buffer = new Buffer(bufferSize);
	}

	public ISignal getSignal() {
		
		return buffer.getNextSignal();
	}

	public void addSignal(ISignal signal){
		buffer.addNewSignal(signal);
	}

	@Override
	public ICOutput getController() {
		return controller;
	}

}

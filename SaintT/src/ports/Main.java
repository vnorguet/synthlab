package ports;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import ports.input.CInput;
import ports.input.ICInput;
import ports.output.COutput;
import ports.output.ICOutput;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame window = new JFrame("Port test");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final ICOutput output = new COutput(10, "test");
		final ICInput input = new CInput("test");
		
		JButton button = new JButton("Plug/Unplug");
		button.addActionListener(new ActionListener() {
			private boolean plugged = false;
			public void actionPerformed(ActionEvent e) {
				if(!plugged){
					input.plug(output);
				}else{
					input.unplug();
				}
				plugged = !plugged;
			}
		});
		
		window.setLayout(new FlowLayout());
		window.add(button);
		window.add((JPanel)output.getPresentation());
		window.add((JPanel)input.getPresentation());
		window.pack();
		
		window.setVisible(true);
	}

}

package ports;

import java.util.List;

public interface ISignal {
	public static final int CAPACITY = 10;
	
	public List<Short> getValues();
	public boolean addValue(Short value);	
	public int addValues(List<Short> values);
}
package sequencer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import modules.generic.ICModule;

public class Sequencer implements ISequencer {
	public static ISequencer getSequencer(){
		if(seq == null){
			seq = new Sequencer();
		}
		return seq;
	}
	
	private static ISequencer seq;
	
	private Sequencer(){}
	
	private List<ICModule> moduleList = new LinkedList<ICModule>();
	HashMap<ICModule, Boolean> checkList = new HashMap<ICModule, Boolean>();

	public void addModule(ICModule module) {
		moduleList.add(module);
	}

	public void removeModule(ICModule module) {
		moduleList.remove(module);
	}

	private boolean checkLoops(ICModule module) {
		checkList.put(module, true);
		if (module.getInputModules().contains(module))
			return true;
		for (ICModule currentModule : module.getInputModules()) {
			if (checkLoops(currentModule))
				return true;
		}
		return false;
	}	

	private void init() {
		checkList = new HashMap<ICModule, Boolean>();
		for (ICModule module : moduleList) {
			checkList.put(module, false);
		}
	}

	public void solve() {
		init();
		for (ICModule module : moduleList) {
			if (!checkList.get(module)){
				if (checkLoops(module)) {
					
				}
			}
		}
	}

}
package sequencer;

import modules.generic.ICModule;

public interface ISequencer{
	public void addModule(ICModule module);

	public void removeModule(ICModule module);

	public void solve();
}

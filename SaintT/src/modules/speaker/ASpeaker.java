package modules.speaker;

import modules.algorythm.IAlgo;
import modules.generic.AModule;
import ports.input.IAInput;

//FAT TODO: comnprendre comment marche Amodule, ce que ca fournit.


public class ASpeaker extends AModule implements IASpeaker {
	private int volume;
	private IAlgo algo;
	private IAInput input;

	public ASpeaker() {
		super();
		setName("AModule");
		algo = new AlgoSpeaker();
	}

	@Override
	public void loop() {
		algo.execute();
	}

	// TODO: faire la fonction setVolume, avec notify() pour le controler
	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getVolume() {
		return volume;
	}

	@Override
	public void setInput(IAInput input) {
		this.input = input;
		addInput(input);
		algo.configure(this);
	}

	public IAInput getInput() {
		return input;
	}

}

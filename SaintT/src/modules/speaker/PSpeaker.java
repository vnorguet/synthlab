package modules.speaker;

import java.awt.Color;
import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modules.generic.PModule;
import ports.input.IPInput;

public class PSpeaker extends PModule implements IPSpeaker {

	private static final long serialVersionUID = -5857796644616565225L;

	//private ICSpeaker controle;

	private IPInput input;

	// private IPPotentiometer volume;

	public PSpeaker(ICSpeaker controller) {
		super();
		this.controller = controller;
	}

	public void build() {

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// Panel header
		JPanel header = new JPanel();
		JLabel labelHeader = new JLabel("HP");
		labelHeader.setForeground(Color.WHITE);
		header.add(labelHeader);
		header.setBackground(COLOR_HEADER);

		// Panel container
		JPanel elements = new JPanel();
		elements.setBackground(COLOR_CONTAINER);
		
		elements.add((JPanel) input);
		ImageIcon imgSpeaker = new ImageIcon("res/modules/speaker.png");
		JLabel speaker = new JLabel(imgSpeaker);
		elements.add(speaker);

		// Layout
		add(header);
		add(elements);
	}

	@Override
	public void setInput(IPInput input) {
		this.input = input;
	}

	/*
	 * @Override public void setVolumePotar(IPPotentiometer potentiometer) {
	 * this.volume = potentiometer; }
	 */

	@Override
	public Point getCorrectionIn() {
		return CORRECTION_IN;
	}

	@Override
	public Point getCorrectionOut() {
		return CORRECTION_OUT;
	}

}

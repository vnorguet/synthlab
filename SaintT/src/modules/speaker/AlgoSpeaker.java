package modules.speaker;

import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import modules.algorythm.IAlgo;
import modules.generic.IAModule;
import ports.ISignal;
import ports.input.IAInput;
import ports.output.IAOutput;

public class AlgoSpeaker implements IAlgo {

	private AudioFormat audioFormat = new AudioFormat(IAModule.MAX_FREQ, 8, 1, false, false);
	private byte[] audio;
	private IAInput input;
	private IASpeaker speaker;
	private IAOutput outputSource;
	private ISignal signal;
	private SourceDataLine line;

	@Override
	public void configure(IAModule module) {
		speaker = (IASpeaker) module;
		input = speaker.getInput();

		// Initialisation de la carte
		DataLine.Info info = new DataLine.Info(SourceDataLine.class,
				audioFormat);
		try {
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(audioFormat);
			line.start();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void execute() {
		if (input.isPlugged()) {
			outputSource = input.getSource();
			signal = outputSource.getSignal();
			List<Short> shortlist = signal.getValues();
			audio = new byte[shortlist.size()];
			for (int i = 0; i < shortlist.size(); i++) {
				audio[i] = shortlist.get(i).byteValue();
			}
			line.write(audio, 0, audio.length);
		}
	}
}
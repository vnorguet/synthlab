package modules.speaker;

import ports.input.ICInput;
import ports.output.ICOutput;
import modules.generic.ICModule;

public interface ICSpeaker extends ICModule {

	public IPSpeaker getPresentation();

	public int getVolume();

	public void plugInput(ICOutput output);

	public ICInput getInput();
}

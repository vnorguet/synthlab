package modules.speaker;

import modules.generic.AModule;
import modules.generic.CModule;
import ports.input.CInput;
import ports.input.ICInput;
import ports.output.ICOutput;

public class CSpeaker extends CModule implements ICSpeaker {
	private ICInput input;

	public CSpeaker() {
		super();
		this.presentation = new PSpeaker(this);
		this.abstraction = new ASpeaker();

		abstraction.setController(this);
		input = new CInput("in");
		((ASpeaker) abstraction).setInput(input.getAbstraction()); // sets the
																	// input for
																	// AAudioOutput
		addInput(input); // adds the input in the lists of controller and
							// abstraction

		//volumePotar = new CPotentiometer(0, 100, (short) 0, "Volume"); // TODO: set
																	// this shit
																	// up
		// TODO: faire la fonction setVolume, qui sera lancée par le
		// notify("volumePotar") du potar.
		// TODO: faire la fonction notify(String name) appropriée.

		((IPSpeaker)presentation).setInput(input.getPresentation());
		//((IPSpeaker)presentation).setVolumePotar(volumePotar.getPresentation());
		((IPSpeaker)presentation).build();
		
		((AModule) abstraction).start();
	}

	public IPSpeaker getPresentation() {
		return (IPSpeaker) presentation;
	}

	@Override
	public void notify(String name) {
		throw new UnsupportedOperationException("Not Implemented yet");
	}

	@Override
	public int getVolume() {
		// TODO Auto-generated method stub
		return 0;
	}

	public ICInput getInput() {
		return input;
	}

	public void plugInput(ICOutput output) {
		input.plug(output);
	}

}

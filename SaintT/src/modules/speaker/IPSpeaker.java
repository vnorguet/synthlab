package modules.speaker;

import java.awt.Point;

import modules.generic.IPModule;
import ports.input.IPInput;

public interface IPSpeaker extends IPModule {

	public static final Point CORRECTION_IN = new Point(3,19);
	public static final Point CORRECTION_OUT = new Point(0,0);
	
	public void build();

	public void setInput(IPInput input);

}

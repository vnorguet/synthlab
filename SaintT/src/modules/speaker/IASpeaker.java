package modules.speaker;

import modules.generic.IAModule;
import ports.input.IAInput;

public interface IASpeaker extends IAModule {

	public void setVolume(int volume);

	public int getVolume();

	public void setInput(IAInput input);

	public IAInput getInput();
}

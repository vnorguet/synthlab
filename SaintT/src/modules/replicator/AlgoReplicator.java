package modules.replicator;

import modules.algorythm.IAlgo;
import modules.generic.IAModule;
import ports.ISignal;

public class AlgoReplicator implements IAlgo {

	/**
	 * Abstraction of the Replicator
	 */
	private IAReplicator myRpk;

	/**
	 * Algorithm which replicates three times an input signal
	 */
	public void execute() {

		if (myRpk.getIn().isPlugged()) {
			ISignal signalRpk = myRpk.getIn().getSignal();

			myRpk.getOut1().addSignal(signalRpk);
			myRpk.getOut2().addSignal(signalRpk);
			myRpk.getOut3().addSignal(signalRpk);
		}
	}

	public void configure(IAModule module) {

		this.myRpk = (IAReplicator) module;
	}

}

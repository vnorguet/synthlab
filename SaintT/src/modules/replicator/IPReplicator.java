package modules.replicator;

import java.awt.Point;

import modules.generic.IPModule;
import ports.input.IPInput;
import ports.output.IPOutput;

/** 
* Interface of the presentation that describes essential features of a Replicator 
* presentation for a graphic illustration.
* 
* @author Les Z'Octaves
*/
public interface IPReplicator extends IPModule {
	
	public static final Point CORRECTION_IN = new Point(3,19);
	public static final Point CORRECTION_OUT = new Point(55,20);
	
	/**
	 * Sets the presentation of an input signal
	 * 
	 * @param in
	 *            Presentation of an input
	 */
	public void setIn(IPInput in);

	/**
	 * Sets the presentation of the first output signal
	 * 
	 * @param out
	 *            Presentation of an output
	 */
	public void setOut1(IPOutput out);

	/**
	 * Sets the presentation of the second output signal
	 * 
	 * @param out
	 *            Presentation of an output
	 */
	public void setOut2(IPOutput out);

	/**
	 * Sets the presentation of the third output signal
	 * 
	 * @param out
	 *            Presentation of an output
	 */
	public void setOut3(IPOutput out);

	/**
	 * Builds the presentation of the Replicator
	 */
	public void build();

}

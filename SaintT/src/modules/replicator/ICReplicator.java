package modules.replicator;

import modules.generic.ICModule;

/** 
 * Interface of the control that describes essential features of a Replicator
 * control for a graphic illustration.
 * <ul>
 * <li> should be able to get back the presentation component associated.
 * </ul>
 * 
 * @author Les Z'Octaves
 */
public interface ICReplicator extends ICModule {

	/**
	 * Gets the presentation of the Replicator
	 * 
	 * @return Return the presentation of the Replicator
	 */
	public IPReplicator getPresentation();
}

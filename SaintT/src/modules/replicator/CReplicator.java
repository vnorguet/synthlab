package modules.replicator;

import modules.generic.AModule;
import modules.generic.CModule;
import ports.input.CInput;
import ports.input.ICInput;
import ports.output.COutput;
import ports.output.ICOutput;

/**
 * Control class that implements essential features of a Replicator presentation
 * in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */
public class CReplicator extends CModule implements ICReplicator {

	/**
	 * Presentation of the Replicator
	 */
	private IPReplicator presentation;

	/**
	 * Control of the input signal
	 */
	private ICInput in;

	/**
	 * Control of the first output signal
	 */
	private ICOutput out1;

	/**
	 * Control of the second output signal
	 */
	private ICOutput out2;

	/**
	 * Control of the second output signal
	 */
	private ICOutput out3;

	public CReplicator() {

		super();
		presentation = new PReplicator(this);
		abstraction = new AReplicator();

		in = new CInput("in");
		addInput(in);
		out1 = new COutput(10, "out 1");
		addOutput(out1);
		out2 = new COutput(10, "out 2");
		addOutput(out2);
		out3 = new COutput(10, "out 3");
		addOutput(out3);

		build();
		((AModule) abstraction).start();

	}

	/**
	 * Initialization of the abstraction and presentation. Build of the
	 * presentation Replicator
	 */
	private void build() {

		((IAReplicator)abstraction).setInput(in.getAbstraction());
		this.addInput(in);
		((IAReplicator)abstraction).setOut1(out1.getAbstraction());
		this.addOutput(out1);
		((IAReplicator)abstraction).setOut2(out2.getAbstraction());
		this.addOutput(out2);
		((IAReplicator)abstraction).setOut3(out3.getAbstraction());
		this.addOutput(out3);
		
		presentation.setIn(in.getPresentation());
		presentation.setOut1(out1.getPresentation());
		presentation.setOut2(out2.getPresentation());
		presentation.setOut3(out3.getPresentation());

		presentation.build();
	}

	public IPReplicator getPresentation() {
		return presentation;
	}

	public ICInput getInput() {
		return in;
	}

	public void setInput(ICInput in) {
		this.in = in;
		//addInput(in);
	}

	public ICOutput getOut1() {
		return out1;
	}

	public void setOut1(ICOutput out1) {
		this.out1 = out1;
		//this.
	}

	public ICOutput getOut2() {
		return out2;
	}

	public void setOut2(ICOutput out2) {
		this.out2 = out2;
	}

	public ICOutput getOut3() {
		return out3;
	}

	public void setOut3(ICOutput out3) {
		this.out3 = out3;
	}

	@Override
	public void notify(String name) {

	}

}

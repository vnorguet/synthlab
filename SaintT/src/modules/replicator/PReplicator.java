package modules.replicator;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modules.generic.PModule;
import ports.input.IPInput;
import ports.output.IPOutput;

/**
 * Presentation class that implements essential features of a Replicator
 * presentation in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */
public class PReplicator extends PModule implements IPReplicator {
	private static final long serialVersionUID = 1L;

	/**
	 * Presentation input signal
	 */
	IPInput in;

	/**
	 * Presentation first output signal
	 */
	IPOutput out1;

	/**
	 * Presentation second output signal
	 */
	IPOutput out2;

	/**
	 * Presentation third output signal
	 */
	IPOutput out3;

	public PReplicator(ICReplicator controller) {

		super();
		this.controller = controller;

	}

	public void setIn(IPInput in) {
		this.in = in;
	}

	public void setOut1(IPOutput out) {
		this.out1 = out;
	}

	public void setOut2(IPOutput out) {
		this.out2 = out;
	}

	public void setOut3(IPOutput out) {
		this.out3 = out;
	}

	public void build() {

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// Panel header
		JPanel header = new JPanel();
		JLabel labelHeader = new JLabel("Replicator");
		labelHeader.setForeground(Color.WHITE);
		header.add(labelHeader);
		header.setBackground(COLOR_HEADER);

		// Panel container
		JPanel elements = new JPanel();
		elements.setBackground(COLOR_CONTAINER);

		JPanel listOut = new JPanel();
		listOut.setLayout(new GridLayout(3, 1));
		listOut.setBackground(COLOR_CONTAINER);

		listOut.add((JPanel) out1);
		listOut.add((JPanel) out2);
		listOut.add((JPanel) out3);

		elements.add((JPanel) in);
		elements.add(listOut);

		// Layout
		add(header);
		add(elements);
	}

	@Override
	public Point getCorrectionIn() {
		return CORRECTION_IN;
	}

	@Override
	public Point getCorrectionOut() {
		return CORRECTION_OUT;
	}
}

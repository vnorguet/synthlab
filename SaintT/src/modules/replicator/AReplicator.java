package modules.replicator;

import modules.generic.AModule;
import ports.input.IAInput;
import ports.output.IAOutput;

/**
 * Abstraction class that implements the interface IAReplicator and provides essential
 * features of a Replicator.
 * 
 * @author Les Z'Octaves
 */
public class AReplicator extends AModule implements IAReplicator {

	/**
 	* Abstraction of the first output signal
 	*/
	IAOutput out1;
	
	/**
 	* Abstraction of the second output signal
 	*/
	IAOutput out2;
	
	/**
 	* Abstraction of the third output signal
 	*/
	IAOutput out3;

	/**
 	* Abstraction of the input signal
 	*/
	IAInput in;

	public AReplicator() {
		super();
		algorythm = new AlgoReplicator();
		algorythm.configure(this);
	}

	public void setInput(IAInput in) {

		this.in = in;

	}

	public void setOut1(IAOutput out) {

		out1 = out;
	}

	public void setOut2(IAOutput out) {

		out2 = out;
	}

	public void setOut3(IAOutput out) {

		out3 = out;
	}

	public IAInput getIn() {

		return in;
	}

	public IAOutput getOut1() {

		return out1;
	}

	public IAOutput getOut2() {

		return out2;
	}

	public IAOutput getOut3() {

		return out3;
	}

}

package modules.replicator;

import ports.input.IAInput;
import ports.output.IAOutput;
import modules.generic.IAModule;

/** 
 * Interface of the abstraction class that describes essential features of a
 * Replicator.
 * 
 * @author Les Z'Octaves
 */
public interface IAReplicator extends IAModule {

	/**
	 * Initialization of the abstraction input signal
	 * 
	 * @param in
	 *            Abstraction of an input signal
	 */
	public void setInput(IAInput in);

	/**
	 * Initialization of the first abstraction output signal
	 * 
	 * @param out
	 *            Abstraction of an output signal
	 */
	public void setOut1(IAOutput out);

	/**
	 * Initialization of the second abstraction output signal
	 * 
	 * @param out
	 *            Abstraction of an output signal
	 */
	public void setOut2(IAOutput out);

	/**
	 * Initialization of the third abstraction output signal
	 * 
	 * @param out
	 *            Abstraction of an output signal
	 */
	public void setOut3(IAOutput out);

	/**
	 * Gets the abstraction of the input signal
	 * 
	 * @return Abstraction of the input signal
	 */
	public IAInput getIn();

	/**
	 * Gets the abstraction of the first output signal
	 * 
	 * @return Abstraction of the first output signal
	 */
	public IAOutput getOut1();

	/**
	 * Gets the abstraction of the second output signal
	 * 
	 * @return Abstraction of the second output signal
	 */
	public IAOutput getOut2();

	/**
	 * Gets the abstraction of the third output signal
	 * 
	 * @return Abstraction of the third output signal
	 */
	public IAOutput getOut3();

}

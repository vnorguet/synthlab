package modules.adsr;

import java.awt.Color;
import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modifier.IPPotentiometer;
import modules.generic.PModule;
import ports.input.IPInput;
import ports.output.IPOutput;

/**
 * Presentation class that implements essential features of an ADSR presentation
 * in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */

public class PADSR extends PModule implements IPADSR {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2890204704178120604L;

	/**
	 * attribute used to access to the presentation of the input signal.
	 */
	IPInput in;

	/**
	 * attribute used to access to the presentation of the output signal.
	 */
	IPOutput out;

	/**
	 * attribute used to access to the presentation of the attack potentiometer.
	 */
	IPPotentiometer attack;

	/**
	 * attribute used to access to the presentation of the decay potentiometer.
	 */
	IPPotentiometer decay;

	/**
	 * attribute used to access to the presentation of the sustain
	 * potentiometer.
	 */
	IPPotentiometer sustain;

	/**
	 * attribute used to access to the presentation of the release
	 * potentiometer.
	 */
	IPPotentiometer release;

	public PADSR(ICADSR controller) {

		super();

		this.controller = controller;
	}

	/**
	 * @see modules.adsr.IPADSR#build()
	 */
	@Override
	public void build() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// Panel header
		JPanel header = new JPanel();
		JLabel labelHeader = new JLabel("ADSR");
		labelHeader.setForeground(Color.WHITE);
		header.add(labelHeader);
		header.setBackground(COLOR_HEADER);
				
		// Panel container
		JPanel elements = new JPanel();
		elements.setBackground(COLOR_CONTAINER);
		elements.add((JPanel) in);
		elements.add((JComponent) attack);
		elements.add((JComponent) decay);
		elements.add((JComponent) sustain);
		elements.add((JComponent) release);
		elements.add((JPanel) out);

		// Layout
		add(header);
		add(elements);
	}

	/**
	 * @see modules.adsr.IPADSR#getIn()
	 */
	@Override
	public IPInput getIn() {
		return in;
	}

	/**
	 * @see modules.adsr.IPADSR#setIn(ports.input.IPInput)
	 */
	@Override
	public void setIn(IPInput in) {
		this.in = in;
	}

	/**
	 * @see modules.adsr.IPADSR#getOut()
	 */
	@Override
	public IPOutput getOut() {
		return out;
	}

	/**
	 * @see modules.adsr.IPADSR#setOut(ports.output.IPOutput)
	 */
	@Override
	public void setOut(IPOutput out) {
		this.out = out;
	}

	/**
	 * @see modules.adsr.IPADSR#getAttack()
	 */
	@Override
	public IPPotentiometer getAttack() {
		return attack;
	}

	/**
	 * @see modules.adsr.IPADSR#setAttack(modifier.IPPotentiometer)
	 */
	@Override
	public void setAttack(IPPotentiometer attack) {
		this.attack = attack;
	}

	/**
	 * @see modules.adsr.IPADSR#getDecay()
	 */
	@Override
	public IPPotentiometer getDecay() {
		return decay;
	}

	/**
	 * @see modules.adsr.IPADSR#setDecay(modifier.IPPotentiometer)
	 */
	@Override
	public void setDecay(IPPotentiometer decay) {
		this.decay = decay;
	}

	/**
	 * @see modules.adsr.IPADSR#getSustain()
	 */
	@Override
	public IPPotentiometer getSustain() {
		return sustain;
	}

	/**
	 * @see modules.adsr.IPADSR#setSustain(modifier.IPPotentiometer)
	 */
	@Override
	public void setSustain(IPPotentiometer sustain) {
		this.sustain = sustain;
	}

	/**
	 * @see modules.adsr.IPADSR#getRelease()
	 */
	@Override
	public IPPotentiometer getRelease() {
		return release;
	}

	/**
	 * @see modules.adsr.IPADSR#setRelease(modifier.IPPotentiometer)
	 */
	@Override
	public void setRelease(IPPotentiometer release) {
		this.release = release;
	}

	@Override
	public Point getCorrectionIn() {
		return CORRECTION_IN;
	}
	
	@Override
	public Point getCorrectionOut() {
		return CORRECTION_OUT;
	}
}

package modules.adsr;

import modules.speaker.CSpeaker;
import modules.speaker.ICSpeaker;
import modules.vco.CVCO;
import modules.vco.ICVCO;

public class MainVCOSpeaker {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ICSpeaker speaker = new CSpeaker();
		ICVCO vco = new CVCO();
		ICADSR adsr = new CADSR();
		
		((IAADSR)adsr.getAbstract()).setAttack(1000);
		
		adsr.plugInput(vco.getOutput());
		speaker.plugInput(adsr.getOutputs().get(0));
	}

}

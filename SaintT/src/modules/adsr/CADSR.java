package modules.adsr;

import ports.input.CInput;
import ports.input.ICInput;
import ports.output.COutput;
import ports.output.ICOutput;
import modifier.CPotentiometer;
import modifier.ICPotentiometer;
import modules.generic.AModule;
import modules.generic.CModule;

/**
 * Control class that implements essential features of an ADSR presentation
 * in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */

public class CADSR extends CModule implements ICADSR {
	
	/**
    * attribute used to access to the attached presentation.
    */
	private IPADSR presentation;
	
	/**
    * attribute used to access to the control of the attack potentiometer.
    */
	private ICPotentiometer attack;
	
	/**
    * attribute used to access to the control of the attack potentiometer.
    */
	private ICPotentiometer decay;
	
	/**
    * attribute used to access to the control of the attack potentiometer.
    */
	private ICPotentiometer sustain;
	
	/**
    * attribute used to access to the control of the attack potentiometer.
    */
	private ICPotentiometer release;
	
	/**
    *  attribute used to access to the control of the input signal.
	*/
	private ICInput in;
	
	/**
    *  attribute used to access to the control of the output signal.
	*/
	private ICOutput out;
	
	/**
    * Build an control component of the ADSR in order to be graphically visualized.
    */
	public CADSR(){
		abstraction = new AADSR();
		presentation = new PADSR(this);
		
		// Ports
		in = new CInput("in");
		out = new COutput(10, "out");
		
		// Buttons
		attack = new CPotentiometer(ATTACK_MIN, ATTACK_MAX, ATTACK_DEFAULT, "Attack");
		attack.addModuleListener(this);
		decay = new CPotentiometer(DECAY_MIN, DECAY_MAX, DECAY_DEFAULT, "Decay");
		decay.addModuleListener(this);
		sustain = new CPotentiometer(SUSTAIN_MIN, SUSTAIN_MAX, SUSTAIN_DEFAULT, "Sustain");
		sustain.addModuleListener(this);
		release = new CPotentiometer(RELEASE_MIN, RELEASE_MAX, RELEASE_DEFAULT, "Release");
		release.addModuleListener(this);
	
		// Build
		build();
		((AModule)abstraction).start();
	}
	
	private void build(){
		((IAADSR)abstraction).setIn(in.getAbstraction());
		this.addInput(in);
		((IAADSR)abstraction).setOut(out.getAbstraction());
		this.addOutput(out);
		((IAADSR)abstraction).setAttack(attack.getValue());
		((IAADSR)abstraction).setDecay(decay.getValue());
		((IAADSR)abstraction).setSustain(sustain.getValue());
		((IAADSR)abstraction).setRelease(release.getValue());
		
		presentation.setAttack(attack.getPresentation());
		presentation.setDecay(decay.getPresentation());
		presentation.setSustain(sustain.getPresentation());
		presentation.setRelease(release.getPresentation());
		presentation.setIn(in.getPresentation());
		presentation.setOut(out.getPresentation());
		presentation.build();
	}

	/**
	 * @see modules.generic.ICModule#notify(java.lang.String)
	 */
	@Override
	public void notify(String name) {
		switch(name){
		case "Attack" : ((IAADSR)abstraction).setAttack(attack.getValue()); break;
		case "Decay" : ((IAADSR)abstraction).setDecay(decay.getValue()); break;
		case "Sustain" : ((IAADSR)abstraction).setSustain(sustain.getValue()); break;
		case "Release" : ((IAADSR)abstraction).setRelease(release.getValue());
		}
	}

	/**
	 * @see modules.generic.CModule#getPresentation()
	 */
	@Override
	public IPADSR getPresentation(){
		return presentation;
	}

	@Override
	public void plugInput(ICOutput source) {
		in.plug(source);
		
	}
}
package modules.adsr;

import ports.input.IAInput;
import ports.output.IAOutput;
import modules.generic.IAModule;

/**
 * Interface of the abstraction class that describes essential features of an
 * ADSR.
 * 
 * @author Les Z'Octaves
 */

public interface IAADSR extends IAModule {

	/**
	 * Sets the abstraction of the output signal.
	 * 
	 * @param out
	 *            the new abstraction value of the output signal to associate
	 */
	public void setOut(IAOutput out);

	/**
	 * Returns the abstraction of the output signal.
	 * 
	 * @return the abstraction of the output signal
	 */
	public IAOutput getOut();

	/**
	 * Sets the abstraction of the input signal.
	 * 
	 * @param in
	 *            the new abstraction value of the input signal to associate
	 */
	public void setIn(IAInput in);

	/**
	 * Returns the abstraction of the input signal.
	 * 
	 * @return the abstraction of the input signal
	 */
	public IAInput getIn();

	/**
	 * Returns the attack value.
	 * 
	 * @return the attack value
	 */
	public float getAttack();

	/**
	 * Sets the attack value.
	 * 
	 * @param attack
	 *            the new attack value to associate
	 */
	public void setAttack(float attack);

	/**
	 * Returns the decay value.
	 * 
	 * @return the decay value
	 */
	public float getDecay();

	/**
	 * Sets the decay value.
	 * 
	 * @param decay
	 *            the new decay value to associate
	 */
	public void setDecay(float decay);

	/**
	 * Returns the sustain value.
	 * 
	 * @return the sustain value
	 */
	public float getSustain();

	/**
	 * Sets the sustain value.
	 * 
	 * @param sustain
	 *            the new sustain value to associate
	 */
	public void setSustain(float sustain);

	/**
	 * Returns the release value.
	 * 
	 * @return the release value
	 */
	public float getRelease();

	/**
	 * Sets the release value.
	 * 
	 * @param release
	 *            the new release value to associate
	 */
	public void setRelease(float release);
}
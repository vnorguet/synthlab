package modules.adsr;

import modules.generic.ICModule;
import ports.output.ICOutput;

/**
 * Interface of the control that describes essential features of an ADSR control
 * for a graphic illustration.
 * <ul>
 * <li>should be able to get back the presentation component associated.
 * </ul>
 * 
 * @author Les Z'Octaves
 */

public interface ICADSR extends ICModule {

	/**
	 * Minimum value of the attack potentiometer.
	 */
	public static final int ATTACK_MIN = 0;

	/**
	 * Maximum value of the attack potentiometer.
	 */
	public static final int ATTACK_MAX = 1000;

	/**
	 * Default value of the attack potentiometer.
	 */
	public static final short ATTACK_DEFAULT = 1;

	/**
	 * Minimum value of the decay potentiometer.
	 */
	public static final int DECAY_MIN = 0;

	/**
	 * Maximum value of the decay potentiometer.
	 */
	public static final int DECAY_MAX = 1000;

	/**
	 * Default value of the decay potentiometer.
	 */
	public static final short DECAY_DEFAULT = 1;

	/**
	 * Minimum value of the sustain potentiometer.
	 */
	public static final int SUSTAIN_MIN = 0;

	/**
	 * Maximum value of the sustain potentiometer.
	 */
	public static final int SUSTAIN_MAX = 100;

	/**
	 * Default value of the sustain potentiometer.
	 */
	public static final short SUSTAIN_DEFAULT = 50;

	/**
	 * Minimum value of the release potentiometer.
	 */
	public static final int RELEASE_MIN = 0;

	/**
	 * Maximum value of the release potentiometer.
	 */
	public static final int RELEASE_MAX = 1000;

	/**
	 * Default value of the release potentiometer.
	 */
	public static final short RELEASE_DEFAULT = 50;

	/**
	 * Returns the presentation component of the ADSR.
	 * 
	 * @return the presentation component of the ADSR associate.
	 */
	public IPADSR getPresentation();
	
	public void plugInput(ICOutput source);
}
package modules.adsr;

import java.util.List;

import ports.ISignal;
import ports.Signal;
import modules.algorythm.IAlgo;
import modules.generic.IAModule;

public class AlgoADSRSynt implements IAlgo {
	private IAADSR client;

	// calcul du pas d'incrementation de l'enveloppe pour conversion en temp
	private float frameRate = 44100F;// Float indispensable sinon =0

	/* env internals */
	private double envMax; // memorisation du max
	// Etat du signal
	private int envState;
	// Temps de l'étape courant
	private double time0;
	// Intensité du signal courant
	private double envLevel;

	/* Constantes d'etat */
	private static final int ATTACK = 0;
	private static final int DECAY = 1;
	private static final int SUSTAIN = 2;
	private static final int RELEASE = 3;

	/* Constante pour le temps de sustain */
	private static final float SUSTAINTIME = 0.5F;
	
	private List<Short> values;
	private ISignal inputSignal;
	ISignal outputSignal;

	public AlgoADSRSynt(String name) {
		super();
		// Initialisation de l'état et du temps
		envState = 0;
		time0 = 0.0;
	}

	public int traite(int i) {
		double putSignal = 0, tmp;
		// Incrémentation
		time0 += 1 / frameRate;

		switch (envState) {
		// Attack
		case ATTACK: {
			// de 0 a max
			if (client.getAttack() == 0.0)
				envLevel = 1.0;
			else
				envLevel = (time0 / client.getAttack());
			putSignal = envLevel;
			if (time0 >= client.getAttack()) {
				time0 = 0.00;
				envState = DECAY;
				envMax = putSignal;
			}
			break;
		}
		// Decay
		case DECAY: {
			if (client.getDecay() == 0.0)
				envLevel = 1.0;
			else
				envLevel = (time0 / client.getDecay());
			putSignal = client.getSustain() * 2 + envLevel
					* (client.getSustain() - envMax);
			if (time0 >= client.getDecay()) {
				time0 = 0.00;
				envState =SUSTAIN;
				envMax = putSignal;
			}
			break;
		}
		// Sustain
		case SUSTAIN: {
			envLevel = client.getSustain();
			putSignal = client.getSustain();
			if (time0 >= SUSTAINTIME) {
				time0 = 0.00;
				envState = RELEASE;
				envMax = putSignal;
			}
			break;
		}
		// Release
		case RELEASE: {
			if (client.getSustain() == 0.0)
				envLevel = 1.0;
			else
				envLevel = ((time0 / SUSTAINTIME * 1000) / 2);
			putSignal = client.getSustain() * envLevel;

			if (time0 >= client.getRelease()) {
				time0 = 0.00;
				envState = ATTACK;
			}
			break;
		}
		}

		// Renvoi du signal
		tmp = (int) (putSignal * i);
		if (tmp > Short.MAX_VALUE)
			tmp = Short.MAX_VALUE;
		else if (tmp < Short.MIN_VALUE)
			tmp = Short.MIN_VALUE;
		return (short) tmp;
	}

	@Override
	public void execute() {
		if (client.getIn() != null) {
			if (client.getIn().isPlugged()) {
				inputSignal = client.getIn().getSignal();
				outputSignal = new Signal();

				values = inputSignal.getValues();
				for (int i = 0; i < ISignal.CAPACITY; i++) {
					short EnvIn = (short) values.get(i);
					outputSignal.addValue((short) traite(EnvIn));
				}
				client.getOut().addSignal(outputSignal);
			}
		}
	}

	@Override
	public void configure(IAModule module) {
		this.client = (IAADSR) module;
	}

}

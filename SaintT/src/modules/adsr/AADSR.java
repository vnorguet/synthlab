package modules.adsr;

import modules.generic.AModule;
import ports.input.IAInput;
import ports.output.IAOutput;

/**
 * Abstraction class that implements the interface IAADSR and provides essential
 * features of an ADSR.
 * 
 * @author Les Z'Octaves
 */

public class AADSR extends AModule implements IAADSR {

	/**
	 * Value of the attack.
	 */
	private float attack;

	/**
	 * Value of the decay.
	 */
	private float decay;

	/**
	 * Value of the sustain
	 */
	private float sustain;

	/**
	 * Value of the release.
	 */
	private float release;

	/**
	 * Abstraction of the input signal.
	 */
	private IAInput in;

	/**
	 * Abstraction of the output signal.
	 */
	private IAOutput out;

	/**
	 * Build an abstraction component of the ADSR.
	 */
	public AADSR() {
		super();
		algorythm = new AlgoADSRSynt("tptp");
		algorythm.configure(this);
	}

	/**
	 * @see modules.adsr.IAADSR#getIn()
	 */
	@Override
	public IAInput getIn() {
		return in;
	}

	/**
	 * @see modules.adsr.IAADSR#setIn(ports.input.IAInput)
	 */
	@Override
	public void setIn(IAInput in) {
		this.in = in;
	}

	/**
	 * @see modules.adsr.IAADSR#getOut()
	 */
	@Override
	public IAOutput getOut() {
		return out;
	}

	/**
	 * @see modules.adsr.IAADSR#setOut(ports.output.IAOutput)
	 */
	@Override
	public void setOut(IAOutput out) {
		this.out = out;
	}

	/**
	 * @see modules.adsr.IAADSR#getAttack()
	 */
	@Override
	public synchronized float getAttack() {
		return attack;
	}

	/**
	 * @see modules.adsr.IAADSR#setAttack(float)
	 */
	@Override
	public synchronized void setAttack(float attack) {
		if (attack > 1000)
			this.attack = 1000 / 1000;
		else if (attack < 0)
			this.attack = 0 / 1000;
		else
			this.attack = attack / 1000;
	}

	/**
	 * @see modules.adsr.IAADSR#getDecay()
	 */
	@Override
	public synchronized float getDecay() {
		return decay;
	}

	/**
	 * @see modules.adsr.IAADSR#setDecay(float)
	 */
	@Override
	public synchronized void setDecay(float decay) {
		if (decay > 1000)
			this.decay = 1000 / 1000;
		else if (decay < 0)
			this.decay = 0 / 1000;
		else
			this.decay = decay / 1000;
	}

	/**
	 * @see modules.adsr.IAADSR#getSustain()
	 */
	@Override
	public synchronized float getSustain() {
		return sustain/100;
	}

	/**
	 * @see modules.adsr.IAADSR#setSustain(float)
	 */
	@Override
	public synchronized void setSustain(float sustain) {
		if (sustain > 1)this.sustain = 1;
		else if (sustain < 0)this.sustain = 0;
		else this.sustain = sustain;


	}

	/**
	 * @see modules.adsr.IAADSR#getRelease()
	 */
	@Override
	public synchronized float getRelease() {
		return release;
	}

	/**
	 * @see modules.adsr.IAADSR#setRelease(float)
	 */
	@Override
	public synchronized void setRelease(float release) {
		if (release > 1000)this.release = 1000/1000;
		else if (release < 0)this.release = 0/1000;
		else this.release = release/1000;
	}
}
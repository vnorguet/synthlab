package modules.adsr;

import java.awt.Point;

import modifier.IPPotentiometer;
import modules.generic.IPModule;
import ports.input.IPInput;
import ports.output.IPOutput;

/**
 * Interface of the presentation that describes essential features of an ADSR
 * presentation for a graphic illustration.
 * 
 * @author Les Z'Octaves
 */

public interface IPADSR extends IPModule {

	public static final Point CORRECTION_IN = new Point(3,19);
	public static final Point CORRECTION_OUT = new Point(3,19);
	
	/**
	 * Sets the presentation of the release potentiometer.
	 * 
	 * @param release
	 *            the new presentation of the release potentiometer to associate
	 */
	public void setRelease(IPPotentiometer release);

	/**
	 * Returns the presentation of the release potentiometer.
	 * 
	 * @return the presentation of the release potentiometer.
	 */
	public IPPotentiometer getRelease();

	/**
	 * Sets the presentation of the sustain potentiometer.
	 * 
	 * @param sustain
	 *            the new presentation of the sustain potentiometer to associate
	 */
	public void setSustain(IPPotentiometer sustain);

	/**
	 * Returns the presentation of the sustain potentiometer.
	 * 
	 * @return the presentation of the sustain potentiometer.
	 */
	public IPPotentiometer getSustain();

	/**
	 * Sets the presentation of the decay potentiometer.
	 * 
	 * @param decay
	 *            the new presentation of the decay potentiometer to associate
	 */
	public void setDecay(IPPotentiometer decay);

	/**
	 * Returns the presentation of the decay potentiometer.
	 * 
	 * @return the presentation of the decay potentiometer.
	 */
	public IPPotentiometer getDecay();

	/**
	 * Sets the presentation of the attack potentiometer.
	 * 
	 * @param attack
	 *            the new presentation of the attack potentiometer to associate
	 */
	public void setAttack(IPPotentiometer attack);

	/**
	 * Returns the presentation of the attack potentiometer.
	 * 
	 * @return the presentation of the attack potentiometer.
	 */
	public IPPotentiometer getAttack();

	/**
	 * Sets the presentation of the output signal.
	 * 
	 * @param out
	 *            the new presentation value of the output signal to associate
	 */
	public void setOut(IPOutput out);

	/**
	 * Returns the presentation of the output signal.
	 * 
	 * @return the presentation of the output signal
	 */
	public IPOutput getOut();

	/**
	 * Sets the presentation of the input signal.
	 * 
	 * @param in
	 *            the new presentation value of the input signal to associate
	 */
	public void setIn(IPInput in);

	/**
	 * Returns the presentation of the input signal.
	 * 
	 * @return the presentation of the input signal
	 */
	public IPInput getIn();

	/**
	 * Build an presentation component of the ADSR.
	 */
	public void build();
}
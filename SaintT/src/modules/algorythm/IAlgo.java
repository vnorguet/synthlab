package modules.algorythm;

import modules.generic.IAModule;


public interface IAlgo {

	public void execute();
	void configure(IAModule module);
	
}

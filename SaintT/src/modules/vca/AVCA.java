package modules.vca;

import modules.generic.AModule;
import ports.input.IAInput;
import ports.output.IAOutput;

/**
 * Abstraction class that implements the interface IAVCA and provides essential
 * features of a VCA.
 * 
 * @author Les Z'Octaves
 */
public class AVCA extends AModule implements IAVCA{
	
	/**
 	* Gain value (potentiometer)
 	*/
	private Short gain = 2;
	
	/**
 	* Abstraction of the input signal & AM signal
 	*/
	private IAInput input, am;
	
	/**
 	* Abstraction of the output signal
 	*/
	private IAOutput out;
	
	public AVCA(){
		super();
		setName("AVCA");
		algorythm = new AlgoVCA();
		algorythm.configure(this);
		
	}
	
	public Short getGain() {
		return gain;
	}
	public void setGain(Short gain) {
		this.gain = gain;
	}
	public synchronized IAInput getInput() {
		if (input == null){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return input;
	}
	public synchronized void setInput(IAInput input) {
		this.input = input;
		notify();
	}
	public IAInput getAm() {
		return am;
	}
	public void setAm(IAInput am) {
		this.am = am;
	}
	public IAOutput getOut() {
		return out;
	}
	public void setOut(IAOutput out) {
		this.out = out;
	}
}

package modules.vca;

import modifier.CPotentiometer;
import modifier.ICPotentiometer;
import modules.generic.AModule;
import modules.generic.CModule;
import ports.input.CInput;
import ports.input.ICInput;
import ports.output.COutput;
import ports.output.ICOutput;

/**
 * Control class that implements essential features of a VCA presentation
 * in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */
public class CVCA extends CModule implements ICVCA {

	/**
	 * Presentation of the VCA
	 */
	private IPVCA presentation;

	/**
	 * Control of the input signal & AM signal
	 */
	private ICInput input, am;

	/**
	 * Control of the potentiometer
	 */
	private ICPotentiometer gain;

	/**
	 * Control of the output signal
	 */
	private ICOutput out;

	public CVCA() {
		super();
		abstraction = new AVCA();
		presentation = new PVCA(this);

		input = new CInput("input");
		this.addInput(input);
		am = new CInput("am");
		this.addInput(am);
		out = new COutput(10, "output");
		this.addOutput(out);
		
		gain = new CPotentiometer(GAIN_MIN, GAIN_MAX, this.getGain(), "Gain");
		gain.addModuleListener(this);
		
		build();
		((AModule) abstraction).start();
	}

	/**
	 * Initialization of the abstraction and presentation. Build of the
	 * presentation VCA
	 */
	private void build() {
		((IAVCA)abstraction).setInput(input.getAbstraction());

		((IAVCA)abstraction).setAm(am.getAbstraction());
		((IAVCA)abstraction).setOut(out.getAbstraction());
		((IAVCA)abstraction).setGain(gain.getValue());

		presentation.setGain(gain.getPresentation());
		presentation.setIn(input.getPresentation());
		presentation.setAm(am.getPresentation());
		presentation.setOut(out.getPresentation());
		presentation.build();
	}

	public IPVCA getPresentation() {
		return presentation;
	}

	public Short getGain(){
		return ((IAVCA)abstraction).getGain();
	}
	
	@Override
	public void notify(String name) {
		
		switch (name) {
		case "Gain":
			((IAVCA)abstraction).setGain(gain.getValue());
		}
	}
}

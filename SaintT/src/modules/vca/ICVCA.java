package modules.vca;

import modules.generic.ICModule;

/** 
 * Interface of the control that describes essential features of a VCA
 * control for a graphic illustration.
 * <ul>
 * <li> should be able to get back the presentation component associated.
 * </ul>
 * 
 * @author Les Z'Octaves
 */
public interface ICVCA extends ICModule {

	/**
	 * Minimum value of the gain
	 */
	public static final int GAIN_MIN = 0;

	/**
	 * Maximum value of the gain
	 */
	public static final int GAIN_MAX = 25;

	/**
	 * Presentation of the input signal
	 */
	//public static final short GAIN_DEFAULT = 0;

	/**
	 * Gets the presentation of the VCA
	 * 
	 * @return Return the presentation of the VCA
	 */
	public IPVCA getPresentation();
}

package modules.vca;

import modules.generic.IAModule;
import ports.input.IAInput;
import ports.output.IAOutput;

/** 
 * Interface of the abstraction class that describes essential features of a
 * VCA.
 * 
 * @author Les Z'Octaves
 */
public interface IAVCA extends IAModule {

	
	/**
	 * Initialization of the gain value
	 * 
	 * @param abstraction
	 *            Gain value
	 */
	public void setGain(Short abstraction);

	/**
	 * Initialization of the abstraction input signal
	 * 
	 * @param abstraction
	 *            Abstraction of an input signal
	 */
	public void setInput(IAInput abstraction);

	/**
	 * Initialization of the abstraction input AM signal
	 * 
	 * @param abstraction
	 *            Abstraction of an input AM signal
	 */
	public void setAm(IAInput abstraction);

	/**
	 * Initialization of the abstraction output signal
	 * 
	 * @param abstraction
	 *            Abstraction of an output signal
	 */
	public void setOut(IAOutput abstraction);

	/**
	 * Gets the potentiometer value
	 * 
	 * @return Potentiometer value
	 */
	public Short getGain();

	/**
	 * Gets the abstraction of the input signal
	 * 
	 * @return Abstraction of the input signal
	 */
	public IAInput getInput();

	/**
	 * Gets the abstraction of the input AM signal
	 * 
	 * @return Abstraction of the input AM signal
	 */
	public IAInput getAm();

	/**
	 * Gets the abstraction of the output signal
	 * 
	 * @return Abstraction of the output signal
	 */
	public IAOutput getOut();

}

package modules.vca;

import modules.algorythm.IAlgo;
import modules.generic.IAModule;
import ports.ISignal;
import ports.Signal;

public class AlgoVCA implements IAlgo {
	private IAVCA vca;
	private ISignal input;
	private ISignal gainSignal;

	public AlgoVCA(){}
	
	/**
	 * Algorithm which multiplies an input signal by the gainSignal value
	 */
	@Override
	public void execute() { // S = In * (Gain|Am)
		if (vca.getInput() == null){
			try {
				Thread.sleep(50);
				System.out.println("pouet");
				return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		input = vca.getInput().getSource().getSignal();
		//System.out.println(input);
		if(vca.getAm().isPlugged()) {
			gainSignal = vca.getAm().getSignal();
			vca.getOut().addSignal(Signal.multiplication(input, gainSignal));
		}
		else 
		{
			Signal res = new Signal();
			for(int i=0; i<ISignal.CAPACITY; i++){ 
				Short k = (short)(input.getValues().get(i) * vca.getGain());
				//System.out.println(vca.getGain());
				res.addValue(k);
			}
			vca.getOut().addSignal(res);
			//System.out.println(res.getValues());
		}
	}

	@Override
	public void configure(IAModule module) {
		this.vca = (IAVCA) module;
		//this.input = vca.getInput();
		//this.gainSignal = vca.getAm();
	}

}

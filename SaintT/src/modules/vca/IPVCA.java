package modules.vca;

import java.awt.Point;

import ports.input.IPInput;
import ports.output.IPOutput;
import modifier.IPPotentiometer;
import modules.generic.IPModule;

/** 
* Interface of the presentation that describes essential features of a VCA 
* presentation for a graphic illustration.
* 
* @author Les Z'Octaves
*/
public interface IPVCA extends IPModule {

	public static final Point CORRECTION_IN = new Point(8,44);
	public static final Point CORRECTION_OUT = new Point(3,19);
	
	/**
	 * Sets the presentation of an input signal
	 * 
	 * @param in
	 *            Presentation of an input
	 */
	public void setIn(IPInput in);

	/**
	 * Sets the presentation of an input signal, type AM
	 * 
	 * @param in
	 *            Presentation of an input
	 */
	public void setAm(IPInput am);

	/**
	 * Sets the presentation of a potentiometer
	 * 
	 * @param gain
	 *            Presentation of a potentiometer
	 */
	public void setGain(IPPotentiometer gain);

	/**
	 * Sets the presentation of an output signal
	 * 
	 * @param in
	 *            Presentation of an output signal
	 */
	public void setOut(IPOutput out);

	/**
	 * Builds the presentation of the VCA
	 */
	public void build();
}

package modules.vca;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame window = new JFrame("VCA test");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ICVCA vca = new CVCA();

		/*
		JButton button = new JButton("Plug/Unplug");
		button.addActionListener(new ActionListener() {
			private boolean plugged = false;
			public void actionPerformed(ActionEvent e) {
				if(!plugged){
					input.plug(output);
				}else{
					input.unplug();
				}
				plugged = !plugged;
			}
		});
		*/
		
		window.setLayout(new FlowLayout());
		//window.add(button);
		window.add((JPanel)vca.getPresentation());
//		window.add((JPanel)audio.getPresentation());
		window.pack();
		
		window.setVisible(true);

	}

}

package modules.vca;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modifier.IPPotentiometer;
import modules.generic.PModule;
import ports.input.IPInput;
import ports.output.IPOutput;

/**
 * Presentation class that implements essential features of a VCA presentation
 * in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */
public class PVCA extends PModule implements IPVCA {
	private static final long serialVersionUID = 1L;

	/**
	 * Presentation input signal, am signal
	 */
	IPInput in, am;

	/**
	 * Presentation output signal
	 */
	IPOutput out;

	/**
	 * Presentation potentiometer
	 */
	IPPotentiometer gain;

	public PVCA(ICVCA controller) {
		super();
		this.controller=controller;
	}

	public void setIn(IPInput in) {
		this.in = in;
	}

	public void setAm(IPInput am) {
		this.am = am;
	}

	public void setOut(IPOutput out) {
		this.out = out;
	}

	public void setGain(IPPotentiometer gain) {
		this.gain = gain;
	}

	public void build() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		// Panel header
		JPanel header = new JPanel();
		JLabel labelHeader = new JLabel("VCA");
		labelHeader.setForeground(Color.WHITE);
		header.add(labelHeader);
		header.setBackground(COLOR_HEADER);

		// Panel container
		JPanel elements = new JPanel();
		elements.setBackground(COLOR_CONTAINER);
		
		JPanel inputs = new JPanel();
		inputs.setLayout(new GridLayout(2, 1));
		inputs.setBackground(COLOR_CONTAINER);
		inputs.add((JPanel) in);
		inputs.add((JPanel) am);
		
		elements.add(inputs);
		elements.add((JComponent) gain);
		elements.add((JPanel) out);

		// Layout
		add(header);
		add(elements);
	}
	
	@Override
	public Point getCorrectionIn() {
		return CORRECTION_IN;
	}
	
	@Override
	public Point getCorrectionOut() {
		return CORRECTION_OUT;
	}

}

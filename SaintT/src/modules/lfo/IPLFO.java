package modules.lfo;

import java.awt.Point;

import modifier.IPPotentiometer;
import modifier.IPSwitch;
import modules.generic.IPModule;
import ports.input.IPInput;
import ports.output.IPOutput;

/** 
* Interface of the presentation that describes essential features of a VCO 
* presentation for a graphic illustration.
* 
* @author Les Z'Octaves
*/
public interface IPLFO extends IPModule {
	
	public static final Point CORRECTION_IN = new Point(3,19);
	public static final Point CORRECTION_OUT = new Point(3,19);
	
	/**
	 * Sets the base frequency presentation
	 * 
	 * @param baseFrequency
	 *            Presentation of a potentiometer
	 */
	public void setBaseFrequency(IPPotentiometer baseFrequency);
	
	/**
	 * Sets the modulation presentation
	 * 
	 * @param modulation
	 *            Presentation of a potentiometer
	 */
	public void setModulation(IPPotentiometer modulation);
	
	/**
	 * Sets the signal type presentation
	 * 
	 * @param signalType
	 *            Presentation of a switch
	 */
	public void setSignalType(IPSwitch signalType);
	
	/**
	 * Sets the FM signal presentation
	 * 
	 * @param fm
	 *            Presentation of a FM Signal
	 */
	public void setFm(IPInput fm);
	
	/**
	 * Sets the presentation of an output signal
	 * 
	 * @param out
	 *            Presentation of an output signal
	 */
	public void setOut(IPOutput out);
	
	/**
	 * Builds the presentation of the VCO
	 */
	public void build();
}

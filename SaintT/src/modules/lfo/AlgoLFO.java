package modules.lfo;

import java.util.ArrayList;
import java.util.List;

import ports.ISignal;
import ports.Signal;

import modules.algorythm.IAlgo;
import modules.generic.IAModule;

public class AlgoLFO implements IAlgo {
	
	
	private IALFO vco;
	
	
	private int k;
	private int cpt = 0;
	boolean way = false;

	public AlgoLFO() {
		
	}

	public void execute() { // F = F0 * 2^(k+fm)
		List<Short> res = new ArrayList<Short>();
		if (vco.getBaseFrequency() == 0){return;}
		// Input signal
		if (vco.getFm().isPlugged()) {
			ISignal inputSignal = vco.getFm().getSignal();

			// Square Signal
			if (vco.getSignalType().equals("square")) {
				for (Short f : inputSignal.getValues()) {
					if( k % (int)((IAModule.MAX_FREQ / vco.getBaseFrequency())) == 0){
						way = !way;
						//System.out.println(cpt);
						//System.out.println(IAModule.MAX_FREQ / vco.getBaseFrequency());
						cpt = 0;
					}
					if (way){
						res.add(IAModule.MAX_VOLTAGE);
					}
					else {
						res.add((short) (-1 * IAModule.MAX_VOLTAGE*f));
					}
					k = (k + 1) % IAModule.MAX_FREQ;
					cpt++;
				}
			}
			// Sine Signal
			else if (vco.getSignalType().equals("sinusoid")) {
				for (Short f : inputSignal.getValues()) {
					res.add((short) Math.sin(k * vco.getBaseFrequency() / (2 * Math.PI)));
					k = (k + 1) % IAModule.MAX_FREQ;
				}
			}
			// Triangle Signal
			else if (vco.getSignalType().equals("triangle")) {
				for (Short f : inputSignal.getValues()) {
					res.add((short) (vco.getBaseFrequency() * Math.pow(2, (k)+ f)));
					k = (k + 1) % IAModule.MAX_FREQ;
				}
			}
			// Saw Signal
			else if (vco.getSignalType().equals("saw")) {
				for (Short f : inputSignal.getValues()) {
					res.add((short) (vco.getBaseFrequency() * Math.pow(2, (k)+ f)));
					k = (k + 1) % IAModule.MAX_FREQ;
				}
			}
		}
		// No input signal
		else {
			// Square Signal
			if (vco.getSignalType().equals("square")) {
				for (int i = 0; i < ISignal.CAPACITY; i++) {
					if( k % (int)((IAModule.MAX_FREQ / vco.getBaseFrequency())) == 0){
						way = !way;
						//System.out.println(cpt);
						//System.out.println(IAModule.MAX_FREQ / vco.getBaseFrequency());
						cpt = 0;
					}
					if (way){
						res.add(IAModule.MAX_VOLTAGE);
					}
					else {
						res.add((short) (-1 * IAModule.MAX_VOLTAGE));
					}
					k = (k + 1) % IAModule.MAX_FREQ;
					cpt++;
				}
			}
			// Sine Signal
			else if (vco.getSignalType().equals("sinusoid")) {
				for (int i = 0; i < ISignal.CAPACITY; i++) {
					res.add((short)((IAModule.MAX_VOLTAGE+1) * Math.sin(k * (vco.getBaseFrequency()/IAModule.MAX_FREQ) / (2 * Math.PI))));
					k = (k + 1) % IAModule.MAX_FREQ;
				}
			}
			// Triangle Signal
			else if (vco.getSignalType().equals("triangle")) {
				k = (k + 1) % IAModule.MAX_FREQ;
			}
			// Saw Signal
			else if (vco.getSignalType().equals("saw")) {
				k = (k + 1) % IAModule.MAX_FREQ;
			}
		}
		//System.out.println(res);
		vco.getOutput().addSignal(new Signal(res));
	}

	public void configure(IAModule module) {
		this.vco = (IALFO) module;
	}

}

package modules.lfo;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modifier.IPPotentiometer;
import modifier.IPSwitch;
import modules.generic.PModule;
import ports.input.IPInput;
import ports.output.IPOutput;

/**
 * Presentation class that implements essential features of a VCO presentation
 * in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */
public class PLFO extends PModule implements IPLFO {

	private static final long serialVersionUID = 1L;

	/**
	 * Presentation switch signal type
	 */
	private IPSwitch signalType;

	/**
	 * Presentation Potentiometer
	 */
	private IPPotentiometer baseFrequency, modulation;

	/**
	 * Presentation input signal, fm signal
	 */
	private IPInput fm;

	/**
	 * Presentation output signal
	 */
	private IPOutput out;

	public PLFO(ICLFO controller) {
		super();
		this.controller = controller;
	}

	public void setSignalType(IPSwitch signalType) {
		this.signalType = signalType;
	}

	public void setBaseFrequency(IPPotentiometer baseFrequency) {
		this.baseFrequency = baseFrequency;
	}

	public void setModulation(IPPotentiometer modulation) {
		this.modulation = modulation;
	}

	public void setFm(IPInput fm) {
		this.fm = fm;
	}

	public void setOut(IPOutput out) {
		this.out = out;
	}

	@Override
	public void build() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
				
		// Panel header
		JPanel header = new JPanel();
		JLabel labelHeader = new JLabel("VCO");
		labelHeader.setForeground(Color.WHITE);
		header.add(labelHeader);
		header.setBackground(COLOR_HEADER);

		// Panel elements
		JPanel elements = new JPanel();
		elements.setBackground(COLOR_CONTAINER);

		JPanel potsIn = new JPanel();
		potsIn.setLayout(new GridLayout(2, 1));
		potsIn.setBackground(COLOR_CONTAINER);
		potsIn.add((JComponent) baseFrequency);
		potsIn.add((JComponent) modulation);
		
		JPanel potsOut = new JPanel();
		potsOut.setBackground(COLOR_CONTAINER);
		potsOut.add((JComponent) signalType);
		
		elements.add((JPanel) fm);
		elements.add(potsIn);
		elements.add(potsOut);
		elements.add((JPanel) out);

		// Layout
		add(header);
		add(elements);
	}

	@Override
	public Point getCorrectionIn() {
		return CORRECTION_IN;
	}

	@Override
	public Point getCorrectionOut() {
		return CORRECTION_OUT;
	}
}

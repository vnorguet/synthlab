package modules.lfo;

import java.util.List;

import modules.generic.IAModule;
import ports.input.IAInput;
import ports.output.IAOutput;

/** 
 * Interface of the abstraction class that describes essential features of a
 * VCO.
 * 
 * @author Les Z'Octaves
 */
public interface IALFO extends IAModule {
	
	/**
	 * Initialization of the Base Frequency
	 * 
	 * @param baseFrequency
	 *            Base Frequency value
	 */
	public void setBaseFrequency(Short baseFrequency);
	
	/**
	 * Initialization of the modulation value
	 * 
	 * @param modulation
	 *            modulation value
	 */
	public void setModulation(Short modulation);
	
	/**
	 * Initialization of the signal type
	 * 
	 * @param signalType
	 *            Type of the signal
	 */
	public void setSignalType(String signalType);
	
	/**
	 * Initialization of the abstraction input FM
	 * 
	 * @param fm
	 *            FM value
	 */
	public void setFm(IAInput fm);
	
	/**
	 * Initialization of the abstraction output
	 * 
	 * @param out
	 *            Abstraction output
	 */
	public void setOutput(IAOutput out);
	
	/**
	 * Gets the base frequency value
	 * 
	 * @return Base Frequency value
	 */
	public Short getBaseFrequency();
	
	/**
	 * Gets the modulation value
	 * 
	 * @return Abstraction of the input signal
	 */
	public Short getModulation();
	
	/**
	 * Gets the abstraction of the input FM signal
	 * 
	 * @return Abstraction of the input FM signal
	 */
	public IAInput getFm();
	
	/**
	 * Gets the abstraction of the output signal
	 * 
	 * @return Abstraction of the output signal
	 */
	public IAOutput getOutput();
	
	/**
	 * Gets the signal type
	 * 
	 * @return The signal type
	 */
	public String getSignalType();
	
	/**
	 * Gets the signal types list
	 * 
	 * @return The signal types list
	 */
	public List<String> getSignalTypes();
}

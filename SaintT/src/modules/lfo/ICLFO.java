package modules.lfo;

import ports.output.ICOutput;
import modules.generic.ICModule;

/**
 * Interface of the control that describes essential features of a VCO control
 * for a graphic illustration.
 * <ul>
 * <li>should be able to get back the presentation component associated.
 * </ul>
 * 
 * @author Les Z'Octaves
 */
public interface ICLFO extends ICModule {

	/**
	 * Name of the signal type switch
	 */
	public static final String NAME_SWITCH_SIGNAL_TYPE = "Signal";

	/**
	 * Name of the base frequency potentiometer
	 */
	public static final String NAME_POTENTIOMETER_BASE_FREQUENCY = "Base Freq.";
	
	/**
	 * Minimum value of the base frequency
	 */
	public static final int BASE_FREQUENCY_MIN = 1;
	
	/**
	 * Maximum value of the base frequency
	 */
	public static final int BASE_FREQUENCY_MAX = 5000;

	/**
	 * Name of the modulation potentiometer
	 */
	public static final String NAME_POTENTIOMETER_MODULATION = "Modulation";
	
	/**
	 * Minimum value of the modulation
	 */
	public static final int MODULATION_MIN = -7;

	/**
	 * Maximum value of the modulation
	 */
	public static final int MODULATION_MAX = 7;

	/**
	 * Gets the presentation of the VCO
	 * 
	 * @return Return the presentation of the VCO
	 */
	public IPLFO getPresentation();

	/**
	 * Gets the control of the output
	 * 
	 * @return Return the control of the output
	 */
	public ICOutput getOutput();
}

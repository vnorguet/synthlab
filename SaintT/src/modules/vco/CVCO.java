package modules.vco;

import modifier.CPotentiometer;
import modifier.CSwitch;
import modifier.ICPotentiometer;
import modifier.ICSwitch;
import modules.generic.AModule;
import modules.generic.CModule;
import ports.ISignal;
import ports.input.CInput;
import ports.input.ICInput;
import ports.output.COutput;
import ports.output.ICOutput;

/**
 * Control class that implements essential features of a VCO presentation
 * in order to be handle illustrated graphically.
 * 
 * @author Les Z'Octaves
 */
public class CVCO extends CModule implements ICVCO {
	
	/**
	 * Presentation of the VCO
	 */
	private IPVCO presentation;
	
	/**
	 * Control of the switch signalType
	 */
	private ICSwitch signalType;
	
	/**
	 * Control of the potentiometer
	 */
	private ICPotentiometer baseFrequency, modulation;
	
	/**
	 * Control of the input signal FM
	 */
	private ICInput fm;
	
	/**
	 * Control of the output signal
	 */
	private ICOutput out;
	
	public CVCO(){
		super();
		this.abstraction = new AVCO();
		this.presentation = new PVCO(this);
		abstraction.setController(this);
		
		signalType = new CSwitch(((IAVCO)abstraction).getSignalTypes(), this.getSignalType(), NAME_SWITCH_SIGNAL_TYPE);
		signalType.addModuleListener(this);
		
		baseFrequency = new CPotentiometer(BASE_FREQUENCY_MIN, BASE_FREQUENCY_MAX, this.getBaseFrequency(), NAME_POTENTIOMETER_BASE_FREQUENCY);
		baseFrequency.addModuleListener(this);
		
		modulation = new CPotentiometer(MODULATION_MIN, MODULATION_MAX, this.getModulation(), NAME_POTENTIOMETER_MODULATION);
		modulation.addModuleListener(this);
		
//		abstraction.setBaseFrequency(baseFrequency.getValue());
		
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//      TEMPLATE: ce qu'il faut faire!!!
		
		fm = new CInput("fm");
		((IAVCO)abstraction).setFm(fm.getAbstraction()); // sets the input
		this.addInput(fm);  // adds the input in the lists of controller and abstraction
		
		//		abstraction.setModulation(modulation.getValue());

		out = new COutput(ISignal.CAPACITY, "out");
		((IAVCO)abstraction).setOutput(out.getAbstraction());
		this.addOutput(out);  // adds the output in the lists of controller and abstraction
			
		//TODO: Rico.... fait moi la fonction notify(string)! bordel de bite.
		//on passe pas des putain de potars a une abstraction!!!!!!!!!
		//Ton bordel d'algo marchais juuuuuste pas! tu mettonne il etait set sur sinusoid avec ces conneries!
		
		presentation.setBaseFrequency(baseFrequency.getPresentation());
		presentation.setFm(fm.getPresentation());
		presentation.setModulation(modulation.getPresentation());
		presentation.setOut(out.getPresentation());
		presentation.setSignalType(signalType.getPresentation());
		presentation.build();
		
		((AModule)abstraction).start();
	}
		
	public Short getBaseFrequency(){
		return ((IAVCO)abstraction).getBaseFrequency();
	}
	
	public Short getModulation(){
		return ((IAVCO)abstraction).getModulation();
	}
	
	public String getSignalType(){
		return ((IAVCO)abstraction).getSignalType();
	}
	
	public IPVCO getPresentation(){
		return presentation;
	}

	public void notify(String name) {
		switch(name){
		case NAME_SWITCH_SIGNAL_TYPE : ((IAVCO)abstraction).setSignalType(signalType.getValue()); break;
		case NAME_POTENTIOMETER_BASE_FREQUENCY : ((IAVCO)abstraction).setBaseFrequency(baseFrequency.getValue()); break;
		case NAME_POTENTIOMETER_MODULATION : ((IAVCO)abstraction).setModulation(modulation.getValue());
		}
	}

	
	public ICOutput getOutput() {
		return out;
	}	
}

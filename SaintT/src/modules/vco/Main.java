package modules.vco;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import modules.speaker.CSpeaker;
import modules.speaker.ICSpeaker;

public class Main {

	public static void main(String[] args) {
		JFrame window = new JFrame("VCO test");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ICVCO vco = new CVCO();
		ICSpeaker audio = new CSpeaker();

		/*
		JButton button = new JButton("Plug/Unplug");
		button.addActionListener(new ActionListener() {
			private boolean plugged = false;
			public void actionPerformed(ActionEvent e) {
				if(!plugged){
					input.plug(output);
				}else{
					input.unplug();
				}
				plugged = !plugged;
			}
		});
		*/
		
		window.setLayout(new FlowLayout());
		//window.add(button);
		window.add((JPanel)vco.getPresentation());
//		window.add((JPanel)audio.getPresentation());
		window.pack();
		
		window.setVisible(true);

	}

}

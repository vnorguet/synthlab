package modules.vco;

import java.util.ArrayList;
import java.util.List;

import modules.generic.AModule;
import ports.input.IAInput;
import ports.output.IAOutput;

/**
 * Abstraction class that implements the interface IAVCO and provides essential
 * features of a VCO.
 * 
 * @author Les Z'Octaves
 */
public class AVCO extends AModule implements IAVCO {
	
	/**
	 * Base Frequency Value
	 */
	private Short baseFrequency= 440 ;
	
	/**
	 * Modulation value
	 */
	private Short modulation = 1;
	
	/**
	 * Abstraction of the input FM signal
	 */
	private IAInput fm;
	
	/**
	 * Abstraction of the output signal
	 */	
	private IAOutput output;
	
	/**
	 * Type of the signal
	 */
	private String signalType = "square";
	
	/**
	 * Signal Types list
	 */
	private List<String> signalTypes = new ArrayList<String>();
	
	
	public AVCO(){
		super();
		algorythm = new AlgoVCO();
		algorythm.configure(this);
	
		signalTypes.add("sinusoid");
		signalTypes.add("square");
		signalTypes.add("sawTeeth");
		signalTypes.add("triangle");
	}
	
	public void setBaseFrequency(Short baseFrequency) {
		this.baseFrequency = baseFrequency;
	}
	public void setModulation(Short modulation) {
		this.modulation = modulation;
	}
	public void setFm(IAInput fm) {
		this.fm = fm;
	}
	
	public void setSignalType(String signalType) {
		this.signalType = signalType;
	}

	public Short getBaseFrequency() {
		return baseFrequency;
	}

	public Short getModulation() {
		return modulation;
	}

	public IAInput getFm() {
		return fm;
	}

	public IAOutput getOutput() {
		return output;
	}

	public void setOutput(IAOutput output) {
		this.output = output; 
	}

	public String getSignalType() {
		return signalType;
	}

	public List<String> getSignalTypes() {
		return signalTypes;
	}

}

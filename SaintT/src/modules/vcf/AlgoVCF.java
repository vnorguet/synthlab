package modules.vcf;

import java.util.ArrayList;
import java.util.List;

import ports.ISignal;
import ports.Signal;
import ports.input.IAInput;
import modules.algorythm.IAlgo;
import modules.generic.IAModule;

public class AlgoVCF implements IAlgo {
	private IAVCF vcf;
	private IAInput input;
	private IAInput cutoff;
	
	/** Fr�quence de coupure de base */
	public static final float FC0 = 1000.0f;
	
	/** Le filtre de Moog* */
	float[] lowPassIn = new float[4];

	/** Le filtre de Moog* */
	float[] lowPassOut = new float[4];

	/** LP et HP filtre * */
	float[] tOut = new float[3];

	/** LP et HP filtre * */
	float[] tIn = new float[3];
	
	private double SQRT2 = Math.sqrt(2);
	
	
	public AlgoVCF(){}
	
	@Override
	public void execute() {
		ISignal res = null;
		
		if (vcf.getInput() == null || vcf.getOut() == null){
			try {
				Thread.sleep(50);
				System.out.println("pouet");
				return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		// Input signal
		if (vcf.getFm().getSource() != null) {
			//ISignal inputSignal = vcf.getFm().getSignal();
			// Low Pass
			if (vcf.getFilterType().equals("low")) {
				res = generateLowPass(true);
			}
			// High Pass
			else if (vcf.getFilterType().equals("high")) {

			}
			// Band Pass
			else if (vcf.getFilterType().equals("band")) {

			}
			// Notch
			else if (vcf.getFilterType().equals("notch")) {
			}
		}
		// No input signal
		else {
			// Low Pass
			if (vcf.getFilterType().equals("low")) {
				res = generateLowPass(false);
				System.out.println("pouet");
			}
			// High Pass
			else if (vcf.getFilterType().equals("high")) {

			}
			// Band Pass
			else if (vcf.getFilterType().equals("band")) {

			}
			// Notch
			else if (vcf.getFilterType().equals("notch")) {

			}
		}
		vcf.getOut().addSignal(res);

	}


	@Override
	public void configure(IAModule module) {
		this.vcf = (IAVCF) module;
		
		this.input = this.vcf.getInput();
		this.cutoff = this.vcf.getFm();
		
		//TODO: do better Gain buffer, wich react with an entry. (not yet drawn in GUI)
		List<Short> gainBuffer = new ArrayList<Short>();
		for (int i = 0; i < ISignal.CAPACITY; i++){
			gainBuffer.add((short) 1);
		}
	}
	
	private ISignal generateLowPass(boolean fixedCutOff) {
		
		//buffer d'entr�es
		ArrayList<Short> inBuffer = (ArrayList<Short>) input.getSignal().getValues();
		ArrayList<Short> cutOffBuffer;
		if (fixedCutOff){
			cutOffBuffer = (ArrayList<Short>) cutoff.getSignal().getValues();
			}
		else{
			cutOffBuffer = new ArrayList<Short>();
			for (int i = 0; i < inBuffer.size(); i++) {
				cutOffBuffer.add((short) vcf.getCutOff());
			}
		}
		
		Signal result = new Signal();
		
		//buffer de sortie
		//float[] outBuffer = out.getWritingBuffer().getSamples();
		
		/*
		if (cutOffValue.isConnected()) {
			cutOffBuffer = cutOffValue.getSampleBuffer().getSamples();
		} else {
			cutOffBuffer = new SampleBuffer().getSamples();
		}
		
		if (gainIn.isConnected()) {
			gainBuffer = gainIn.getSampleBuffer().getSamples();
		} else {
			gainBuffer = new SampleBuffer().getSamples();
		}
		*/
		
		float fc = 0.0f;
		float res = 0.0f;
		for(int i = 0; i < ISignal.CAPACITY; i++){
			// d�termination de la fr�quence de coupure.
			fc = (float) (IAVCF.CUTOFF_FREQUENCY_DEFAULT*Math.pow(2.0, cutOffBuffer.get(i)));
			res = 1;
			result.addValue((short)generateLowPassValue(inBuffer.get(i), fc, res));
		}
		return result;
	}

	
	/**
	 * Approximation d'un filtre passe bas.
	 * 
	 * @param input : Amplitude de l'�chantillon en entr�e du filtre.
	 * @param fc : Fr�quence de coupure comprise entre ~0Hz et �chantillonnage/2.
	 * @param res : gain � la r�sonance compris entre ~0.1 et sqrt(2).  
	 * @return : Amplitude de l'�chantillon en sortie du filtre.
	 */
	public float generateLowPassValue(float input, float fc, double res) {
		double r = SQRT2 - res * SQRT2;
		double c = 1.0 / Math.tan((Math.PI * fc) / IAModule.MAX_FREQ);  //maybe just MAX_FREQ
		double a1 = 1.0 / (1.0 + r * c + c * c);
		double a2 = 2 * a1;
		double a3 = a1;
		double b1 = 2.0 * (1.0 - c * c) * a1;
		double b2 = (1.0 - r * c + c * c) * a1;

		tIn[0] = input;
		tOut[0] = (float) (a1 * tIn[0] + a2 * tIn[1] + a3 * tIn[2] - b1
				* tOut[1] - b2 * tOut[2]);

		tIn[2] = tIn[1];
		tIn[1] = tIn[0];

		tOut[2] = tOut[1];
		tOut[1] = tOut[0];
		System.out.println(tOut[0]);

		return tOut[0];
	}


}

package modules.vcf;

import java.awt.Point;

import ports.input.IPInput;
import ports.output.IPOutput;
import modifier.IPPotentiometer;
import modifier.IPSwitch;
import modules.generic.IPModule;

public interface IPVCF extends IPModule {
	public static final Point CORRECTION_IN = new Point(8,94);
	public static final Point CORRECTION_OUT = new Point(3,19);

	public void setFrequency(IPPotentiometer frequency);

	public void setLevel(IPPotentiometer level);

	public void setSignalType(IPSwitch signalType);

	public void setIn(IPInput in);

	public void setFm(IPInput fm);

	public void setOut(IPOutput out);

	public void build();
}

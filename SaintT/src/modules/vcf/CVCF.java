package modules.vcf;

import modifier.CPotentiometer;
import modifier.CSwitch;
import modifier.ICPotentiometer;
import modifier.ICSwitch;
import modules.generic.AModule;
import modules.generic.CModule;
import ports.input.CInput;
import ports.input.ICInput;
import ports.output.COutput;
import ports.output.ICOutput;

public class CVCF extends CModule implements ICVCF {
	private IPVCF presentation;

	private ICSwitch filterType;
	private ICPotentiometer cutOff, level;
	private ICInput in, fm;
	private ICOutput out;

	public CVCF() {
		abstraction = new AVCF();
		presentation = new PVCF(this);

		filterType = new CSwitch(((IAVCF) abstraction).getFilterTypes(), "low",
				"filterType");
		filterType.addModuleListener(this);
		
		cutOff = new CPotentiometer(IAVCF.FREQUENCY_MIN, IAVCF.FREQUENCY_MAX,
				IAVCF.CUTOFF_FREQUENCY_DEFAULT, "cutOff");
		cutOff.addModuleListener(this);
		
		level = new CPotentiometer(IAVCF.LEVEL_MIN, IAVCF.LEVEL_MAX,
				IAVCF.LEVEL_DEFAULT, "level");
		level.addModuleListener(this);
		
		in = new CInput("in");
		fm = new CInput("fm");
		out = new COutput(10, "out");

		build();
		((AModule) abstraction).start();
	}

	private void build() {
		this.addInput(in);
		this.addInput(fm);
		this.addOutput(out);
		((IAVCF) abstraction).setFrequency(IAVCF.CUTOFF_FREQUENCY_DEFAULT);
		((IAVCF) abstraction).setFm(fm.getAbstraction());
		//((IAVCF) abstraction).setInput(in.getAbstraction());
		((IAVCF) abstraction).setLevel(IAVCF.LEVEL_DEFAULT);
		((IAVCF) abstraction).setOut(out.getAbstraction());
		((IAVCF) abstraction).setFilterType(IAVCF.FILTER_DEFAULT);

		presentation.setFrequency(cutOff.getPresentation());
		presentation.setFm(fm.getPresentation());
		presentation.setIn(in.getPresentation());
		presentation.setLevel(level.getPresentation());
		presentation.setOut(out.getPresentation());
		presentation.setSignalType(filterType.getPresentation());
		presentation.build();
	}

	public IPVCF getPresentation() {
		return presentation;
	}
	/*
	public Short getGain(){
		return ((IAVCA)abstraction).getGain();
	}
	public Short getGain(){
		return ((IAVCA)abstraction).getGain();
	}
	public Short getGain(){
		return ((IAVCA)abstraction).getGain();
	}
*/
	@Override
	public void notify(String name) {
		switch (name) {
		case "filterType":
			((IAVCF) abstraction).setFilterType(filterType.getValue());
			break;
		case "cutOff":
			((IAVCF) abstraction).setCutOff(cutOff.getValue());
			break;
		case "level":
			((IAVCF) abstraction).setLevel(level.getValue());
			break;
		}
	}
}

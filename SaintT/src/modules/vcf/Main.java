package modules.vcf;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame window = new JFrame("VCF test");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ICVCF vcf = new CVCF();

		/*
		JButton button = new JButton("Plug/Unplug");
		button.addActionListener(new ActionListener() {
			private boolean plugged = false;
			public void actionPerformed(ActionEvent e) {
				if(!plugged){
					input.plug(output);
				}else{
					input.unplug();
				}
				plugged = !plugged;
			}
		});
		*/
		
		window.setLayout(new FlowLayout());
		//window.add(button);
		window.add((JPanel)vcf.getPresentation());
//		window.add((JPanel)audio.getPresentation());
		window.pack();
		
		window.setVisible(true);

	}

}

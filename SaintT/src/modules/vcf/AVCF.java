package modules.vcf;

import java.util.ArrayList;
import java.util.List;

import modules.generic.AModule;
import ports.input.IAInput;
import ports.output.IAOutput;

public class AVCF extends AModule implements IAVCF {
	private float frequency, level;
	private IAInput in, fm;
	private IAOutput out;
	
	private List<String> filterTypes;
	private String filterType = "low";
	
	private Short cutOff = IAVCF.CUTOFF_FREQUENCY_DEFAULT;
	
	
	public AVCF(){
		algorythm = new AlgoVCF();
		algorythm.configure(this);
		
		filterType = "LP";
		filterTypes = new ArrayList<String>();
		filterTypes.add("low");
		filterTypes.add("high");
		filterTypes.add("band");
		filterTypes.add("notch");
	}

	public List<String> getTypes(){
		return filterTypes;
	}
	
	public float getFrequency() {
		return frequency;
	}

	public void setFrequency(float frequency) {
		this.frequency = frequency;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public IAInput getInput() {
		return in;
	}

	public void setInput(IAInput in) {
		this.in = in;
		algorythm.configure(this);
	}

	public IAInput getFm() {
		return fm;
	}

	public void setFm(IAInput fm) {
		this.fm = fm;
		algorythm.configure(this);
	}

	public IAOutput getOut() {
		return out;
	}

	public void setOut(IAOutput out) {
		this.out = out;
		algorythm.configure(this);
	}

	public String getFilterType() {
		return filterType;
	}

	public List<String> getFilterTypes() {
		return filterTypes;
	}

	public void setFilterType(String type) {
		this.filterType = type;
	}

	public Short getCutOff() {
		return cutOff;
	}

	public void setCutOff(Short cutOff) {
		this.cutOff = cutOff;
	}

	
}

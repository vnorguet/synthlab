package modules.vcf;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modifier.IPPotentiometer;
import modifier.IPSwitch;
import modules.generic.PModule;
import ports.input.IPInput;
import ports.output.IPOutput;

public class PVCF extends PModule implements IPVCF {
	private static final long serialVersionUID = 1L;

	private IPPotentiometer frequency, level;
	private IPSwitch signalType;
	private IPInput in, fm;
	private IPOutput out;

	public PVCF(ICVCF controller) {
		super();
		this.controller = controller;
	}

	public void setFrequency(IPPotentiometer frequency) {
		this.frequency = frequency;
	}

	public void setLevel(IPPotentiometer level) {
		this.level = level;
	}

	public void setSignalType(IPSwitch signalType) {
		this.signalType = signalType;
	}

	public void setIn(IPInput in) {
		this.in = in;
	}

	public void setFm(IPInput fm) {
		this.fm = fm;
	}

	public void setOut(IPOutput out) {
		this.out = out;
	}

	public void build() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// Panel header
		JPanel header = new JPanel();
		JLabel labelHeader = new JLabel("VCF");
		labelHeader.setForeground(Color.WHITE);
		header.add(labelHeader);
		header.setBackground(COLOR_HEADER);

		// Panel container
		JPanel elements = new JPanel();
		elements.setBackground(COLOR_CONTAINER);

		JPanel inputs = new JPanel();
		inputs.setLayout(new GridLayout(2, 1));
		inputs.add((JPanel) in);
		inputs.add((JPanel) fm);

		JPanel pots = new JPanel();
		pots.setLayout(new GridLayout(2, 1));
		pots.setBackground(COLOR_CONTAINER);
		pots.add((JComponent) frequency);
		pots.add((JComponent) level);

		elements.add(inputs);
		elements.add(pots);
		elements.add((JComponent) signalType);
		elements.add((JPanel) out);

		// Layout
		add(header);
		add(elements);
	}

	@Override
	public Point getCorrectionIn() {
		return CORRECTION_IN;
	}

	@Override
	public Point getCorrectionOut() {
		return CORRECTION_OUT;
	}
}

package modules.vcf;

import modules.generic.ICModule;

public interface ICVCF extends ICModule {
	
	public IPVCF getPresentation();
}

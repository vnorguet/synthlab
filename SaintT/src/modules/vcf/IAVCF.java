package modules.vcf;

import java.util.List;

import ports.input.IAInput;
import ports.output.IAOutput;
import modules.generic.IAModule;

public interface IAVCF extends IAModule {

	public static final int FREQUENCY_MIN = 20;
	public static final int FREQUENCY_MAX = 5000;
	public static final short CUTOFF_FREQUENCY_DEFAULT = 1000;
	public static final int LEVEL_MIN = -5;
	public static final int LEVEL_MAX = 5;
	public static final short LEVEL_DEFAULT = 0;
	public static final String FILTER_DEFAULT = "low";

	
	void setFrequency(float freq);

	void setFm(IAInput input);

	void setInput(IAInput input);

	void setLevel(float level);

	void setOut(IAOutput output);

	void setFilterType(String type);
	
	public float getFrequency();
	public float getLevel();
	public String getFilterType();
	public List<String> getFilterTypes();
	public IAInput getFm();
	public IAInput getInput();
	public IAOutput getOut();
	public Short getCutOff();
	public void setCutOff(Short cutOff);


}

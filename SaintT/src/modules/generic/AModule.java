package modules.generic;

import java.util.ArrayList;
import java.util.List;

import modules.algorythm.IAlgo;
import ports.input.IAInput;
import ports.output.IAOutput;

//TODO: change the controls into direct abstractions for better efficiency

public abstract class AModule extends Thread implements IAModule{

	protected ICModule controller;
	protected IAlgo algorythm;

	protected List<IAOutput> outputList;
	protected List<IAInput> inputList;
	
	private boolean running = true;
	
	public AModule(){
		inputList = new ArrayList<IAInput>();
		outputList = new ArrayList<IAOutput>();
	}
	
	public void run() {
		init();
		while(running){
			loop();
		}
	}
	
	public void init(){
		
	}
	
	public void loop() {
		algorythm.execute();
	}
	
	public void setAlgorythm(IAlgo algo) {
		this.algorythm = algo;
		this.algorythm.configure(this);
	}

	public ICModule getController() {
		return controller;
	}

	public void setController(ICModule controller) {
		this.controller = controller;
	}

	public List<IAOutput> getOutputs() {
		return outputList;
	}

	public List<IAInput> getInputs() {
		return inputList;
	}

	public void addOutput(IAOutput out){
		this.outputList.add(out);
	}
	
	public void addInput(IAInput in){
		this.inputList.add(in);
	}
	
	public void stopModule(){
		this.running = false;
	}
//	private void waitForPlug(IAInput plug){
//		while(plug.getSource() == null){
//			try {
//				wait();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//	}
	
	// CRADOOOOOO
	String moduleType;
	public String getType(){
		return this.moduleType;
	}
	//TODO: Changer l'algo du Sequencer!

}

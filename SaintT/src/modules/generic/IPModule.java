package modules.generic;

import java.awt.Color;
import java.awt.Point;

import modules.generic.ICModule;

public interface IPModule {

	public static final Color COLOR_HEADER = new Color(102, 130, 178);
	public static final Color COLOR_CONTAINER = new Color(225, 228, 242);

	public ICModule getControl();

	public abstract Point getCorrectionIn();

	public abstract Point getCorrectionOut();
}

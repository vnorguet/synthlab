package modules.generic;

import java.util.ArrayList;
import java.util.List;

import ports.input.ICInput;
import ports.output.ICOutput;


public abstract class CModule implements ICModule {

	protected IAModule abstraction;
	protected IPModule presentation;
	protected List<ICInput> inputList;
	protected List<ICOutput> outputList;
	
	public CModule(){
		inputList = new ArrayList<ICInput>();
		outputList = new ArrayList<ICOutput>();
	}

	public IPModule getPresentation() {
		return this.presentation;
	}

	public IAModule getAbstract() {
		return this.abstraction;
	}

	public List<ICModule> getInputModules() {
		List<ICModule> result = new ArrayList<ICModule>();
		for (ICInput input : this.inputList) {
			result.add(input.getSource().getModule());
		}
		return result;
	}
	
	public void addOutput(ICOutput out){
		this.outputList.add(out);
		this.abstraction.addOutput(out.getAbstraction());
	}

	public void addInput(ICInput in){
		this.inputList.add(in);		
		//System.out.println(abstraction);
		this.abstraction.addInput(in.getAbstraction());
	}

	public List<ICOutput> getOutputs(){
		return outputList;
	}
	
	public List<ICInput> getInputs(){
		return inputList;
	}
	
	public void stopModule(){
		abstraction.stopModule();
	}

}

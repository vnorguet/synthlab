package modules.generic;

import java.util.List;

import modules.algorythm.IAlgo;
import ports.input.IAInput;
import ports.output.IAOutput;
 
public interface IAModule extends Runnable{

	final static int MAX_FREQ = 44100; // freqs available 8000,11025,16000,22050,44100
	final static Short MAX_VOLTAGE = 5;  // was 5
	final static Short VOLT_PER_OCTAVE = 1;
	
	public void run();
	abstract void loop();
	
	public ICModule getController();
	public void setController(ICModule controller);
	public void setAlgorythm(IAlgo algorythm);
	public List<IAOutput> getOutputs();
	public List<IAInput> getInputs();
	public void addInput(IAInput in);
	public void addOutput(IAOutput in);

		//CRADOOOO
	public String getType();

	public void stopModule();
		
}
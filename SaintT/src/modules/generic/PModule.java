package modules.generic;

import java.awt.Point;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

public abstract class PModule extends JPanel implements IPModule {
	private static final long serialVersionUID = 1L;

	protected ICModule controller;

	public PModule() {
		setBorder(new javax.swing.border.BevelBorder(BevelBorder.RAISED));
	}

	public ICModule getControl() {
		return controller;
	}

	@Override
	public abstract Point getCorrectionIn();

	@Override
	public abstract Point getCorrectionOut();
}

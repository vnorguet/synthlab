package modules.generic;

import java.util.List;

import ports.input.ICInput;
import ports.output.ICOutput;

public interface ICModule {

	public IPModule getPresentation();
	public IAModule getAbstract();

	public List<ICModule> getInputModules();
	public void addInput(ICInput in);
	public void addOutput(ICOutput out);
	public void notify(String name);
	public List<ICOutput> getOutputs();
	public List<ICInput> getInputs();
	
	public void stopModule();
}


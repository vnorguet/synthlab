package app.core;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import app.actionlisteners.FileActionListener;
import app.modules.IPModulePanel;
import app.work.IPWorkPanel;

public class PApp extends JFrame implements IPApp {
	private static final long serialVersionUID = 1L;

	private IPModulePanel modulePanel;
	private IPWorkPanel workPanel;

	public PApp() {
		super("Saint T");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(Toolkit.getDefaultToolkit().getScreenSize());
		setPreferredSize(getSize());
		//getContentPane().setLayout(new FlowLayout());
	}

	@Override
	public void build() {
		// Menu
		JMenuBar menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenuItem m = null;
		ActionListener fileActionListener = new FileActionListener(this);
		for (String s : FILE_ACTIONS) {
			m = new JMenuItem(s);
			m.addActionListener(fileActionListener);
			file.add(m);
		}
		menuBar.add(file);
		setJMenuBar(menuBar);

		// ModulePanel
		// JScrollPane mod = new JScrollPane((JPanel)modulePanel);
		// getContentPane().add(mod);
		getContentPane().add((JPanel) modulePanel,BorderLayout.WEST);

		// WorkPanel
		// JScrollPane wrk = new JScrollPane((JPanel)workPanel);
		// getContentPane().add(wrk);
		getContentPane().add((JPanel) workPanel,BorderLayout.CENTER);
		
		// Display
		pack();
		setVisible(true);
	}

	public void setModulePanel(IPModulePanel modulePanel) {
		this.modulePanel = modulePanel;
	}

	public void setWorkPanel(IPWorkPanel workPanel) {
		this.workPanel = workPanel;
	}
}

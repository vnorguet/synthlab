package app.core;

import app.modules.IPModulePanel;
import app.work.IPWorkPanel;

public interface IPApp {
	public static final String[] FILE_ACTIONS = { "New", "Load", "Save", "Quit" };
	
	public void build();

	public void setModulePanel(IPModulePanel presentation);

	public void setWorkPanel(IPWorkPanel presentation);

}

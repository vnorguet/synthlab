package app.core;

import app.modules.CModulePanel;
import app.modules.ICModulePanel;
import app.work.CWorkPanel;
import app.work.ICWorkPanel;

public class CApp implements ICApp {
	private IPApp presentation;
	
	private ICModulePanel modulePanel;
	private ICWorkPanel workPanel;
	
	public CApp(){
		presentation = new PApp();
		
		modulePanel = new CModulePanel(this);
		workPanel = new CWorkPanel();
		
		build();
	}
	
	private void build(){
		presentation.setModulePanel(modulePanel.getPresentation());
		presentation.setWorkPanel(workPanel.getPresentation());
		presentation.build();
	}

	@Override
	public void addModule(String moduleName) {
		workPanel.addModule(moduleName);		
	}
}

package app.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import app.modules.IPModulePanel;

public class ModuleActionListener implements ActionListener {
	private IPModulePanel invoker;
	
	public ModuleActionListener(IPModulePanel invoker) {
		this.invoker = invoker;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
			case "VCO" : invoker.getControl().createModule("VCO"); break;
			case "VCA" : invoker.getControl().createModule("VCA"); break;
			case "VCF" : invoker.getControl().createModule("VCF");break;
			case "ADSR" : invoker.getControl().createModule("ADSR"); break;
			case "Replicator" : invoker.getControl().createModule("Replicator");break;
			case "HP" : invoker.getControl().createModule("HP");break;
			case "File" : invoker.getControl().createModule("File");
		}
	}
}
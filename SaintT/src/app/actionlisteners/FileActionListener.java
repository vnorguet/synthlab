package app.actionlisteners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import app.core.IPApp;

public class FileActionListener implements ActionListener {
	IPApp invoker;
	
	public FileActionListener(IPApp invoker) {
		this.invoker = invoker;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "New" : break;
		case "Load" : break;
		case "Save" : break;
		case "Quit" : ((JFrame)invoker).dispose(); System.exit(0);
		}
	}

}

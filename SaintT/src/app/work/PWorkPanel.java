package app.work;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import modules.generic.IPModule;
import ports.input.ICInput;
import ports.output.ICOutput;

public class PWorkPanel extends JPanel implements IPWorkPanel,
		MouseMotionListener, MouseListener {
	private List<IWire> wires;
	private Map<IWire, List<IPModule>> connections;
	private IPModule currentModule;
	private boolean wireInPluggingPhase;
	
	private static final long serialVersionUID = 1L;
	

	public PWorkPanel() {
		wires = new LinkedList<IWire>();
		connections = new HashMap<IWire, List<IPModule>>();
		wireInPluggingPhase = false;
		// setSize(500, 400);
		// setPreferredSize(getSize());
		setLayout(null);
		// setBackground(Color.GRAY);
		addMouseMotionListener(this);
		addMouseListener(this);
	}

	@Override
	public void add(IPModule presentation) {
		/*
		 * try { Robot r = new Robot();
		 * r.mouseMove(getLocation().x,getLocation().y); } catch (AWTException
		 * e) { e.printStackTrace(); }
		 */
		currentModule = presentation;
		add((JPanel) currentModule, 0);
		Dimension size = ((JPanel) currentModule).getPreferredSize();
		((JPanel) currentModule).setBounds(0, 0, size.width, size.height);
		validate();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (currentModule != null) {
			Dimension size = ((JPanel) currentModule).getPreferredSize();
			((JPanel) currentModule).setBounds(e.getX(), e.getY(), size.width,
					size.height);
			repaint();
			validate();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		boolean cancel = false;
		if (currentModule != null) {
			currentModule = null;
		} else {
			IPModule module = null;
			// Gets the module under the cursor
			JPanel comp = (JPanel) getComponentAt(e.getPoint());
			//System.out.println(comp);
			try {
				// Casts the Component as a IPModule, raises a
				// ClassCastException
				module = (IPModule) comp;
			} catch (ClassCastException cce) {
				//System.err.println("Impossible to suppress a module.");
				cancel = true;
			}
			if(!cancel){
			if (e.getButton() == MouseEvent.BUTTON1) {
				// currentModule = module;
			} else if ((e.getButton() == MouseEvent.BUTTON3) && (module != null)) {
				boolean found = false;
				
				//System.out.println("Looking for outputs");
				// Tests if the click was on an output
				for (int i = 0; (!found) && (i<module.getControl().getOutputs().size()) ; i++) {
					ICOutput ico = module.getControl().getOutputs().get(i);
					Point relativeToOutput = new Point();
					relativeToOutput.setLocation(e.getX() - comp.getX() - ((JPanel) ico.getPresentation()).getX(),
							e.getY() - comp.getY() - ((JPanel) ico.getPresentation()).getY());
					if ((relativeToOutput.x != 0) && (relativeToOutput.y != 0)
							&& (ico.getPresentation()).contains(relativeToOutput,module.getCorrectionOut()) && (!ico.isPlugged())) {
						for(IWire w : wires){
							if(connections.get(w).get(0).getControl().getOutputs().contains(ico)){
								w.unplugAll();
								connections.remove(w);
								wires.remove(w);
								found = true;
								break;
							}
						}
					}
				}
				
				if(!found){
					//System.out.println("Looking for inputs");
					// Tests if the click was on an input
					for (int i = 0; (!found) && (i<module.getControl().getInputs().size()) ; i++) {
						ICInput ici = module.getControl().getInputs().get(i);
						Point relativeToInput = new Point();
						relativeToInput.setLocation(e.getX() - comp.getX() - ((JPanel) ici.getPresentation()).getX(),
								e.getY() - comp.getY() - ((JPanel) ici.getPresentation()).getY());
						if ((relativeToInput.x != 0) && (relativeToInput.y != 0)
								&& (ici.getPresentation()).contains(relativeToInput,module.getCorrectionIn())) {
							for(IWire w : wires){
								if(connections.get(w).get(0).getControl().getOutputs().contains(ici)){
									w.unplugAll();
									connections.remove(w);
									wires.remove(w);
									found = true;
									break;
								}
							}
						}
					}
				}
				if(!found){
					//System.out.println("No in/out found, supressing module");
					module.getControl().stopModule();
					for(int i = 0; i<wires.size(); ){
						IWire w = wires.get(i);
						if(connections.get(w).contains(module)){
							w.unplugAll();
							connections.remove(w);
							wires.remove(w);
						}else{
							i++;
						}
					}
					remove(comp);
					repaint();
				}
			}
			//System.out.println("After remove : " + connections);
		}
			}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (currentModule == null) {
			if (e.getButton() == MouseEvent.BUTTON1) { // left click
				// Gets the module under the cursor
				JPanel comp = (JPanel) getComponentAt(e.getPoint());
				try {
					// Casts the Component as a IPModule, raises a ClassCastException
					IPModule module = (IPModule) comp;
					// Creates a Point that represents the relative position of the mouse in the IPModule
					for (ICOutput ico : module.getControl().getOutputs()) {
						Point relativeToOutput = new Point();
						relativeToOutput.setLocation(e.getX() - comp.getX() - ((JPanel) ico.getPresentation()).getX(),
								e.getY() - comp.getY() - ((JPanel) ico.getPresentation()).getY());
						if ((relativeToOutput.x != 0) && (relativeToOutput.y != 0)
								&& (ico.getPresentation()).contains(relativeToOutput,module.getCorrectionOut()) && (!ico.isPlugged())) {
							wireInPluggingPhase = true;
							IWire w = new Wire(ico);
							wires.add(0, w);
							List<IPModule> m = new ArrayList<IPModule>();
							m.add(module);
							connections.put(w, m);
							//System.out.println("Conections : " + connections);
							break;
						}
					}
				} catch (ClassCastException cce) {
					//System.err.println("Ce n'est pas un module");
				}
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		boolean cancel = true;
		if (currentModule == null) {
			if (e.getButton() == MouseEvent.BUTTON1) { // left click
			// Gets the module under the cursor
			JPanel comp = (JPanel) getComponentAt(e.getPoint());
			try {
				// Casts the Component as a IPModule, raises a
				// ClassCastException
				IPModule module = (IPModule) comp;
				// Creates a Point that represents the relative position of
				// the mouse in the IPModule

				for (ICInput ici : module.getControl().getInputs()) {
					Point relativeToInput = new Point();
					relativeToInput.setLocation(
							e.getX()
									- comp.getX()
									- ((JPanel) ici.getPresentation())
											.getX(),
							e.getY()
									- comp.getY()
									- ((JPanel) ici.getPresentation())
											.getY());
					if (((ici.getPresentation()).contains(relativeToInput,
							module.getCorrectionIn()))) {
							IWire w = wires.get(0);
							if ((wires.size() > 0) && (w.setIn(ici))) {
								add((Wire)w);
								List<IPModule> m = connections.get(w);
								m.add(module);
								//System.out.println("M : " + m);
								//System.out.println("Coooooonections : " + connections);
								cancel = false;
								wireInPluggingPhase = false;
								break;
							}
						}
					}
				} catch (ClassCastException cce) {
					//System.err.println("Ce n'est pas un moduleux");
				}

				if (wires.size() > 0 && cancel && wireInPluggingPhase) {
					wireInPluggingPhase = false;
					IWire w = wires.get(0); 
					w.unplugAll();
					connections.remove(w);
					wires.remove(w);
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}
}

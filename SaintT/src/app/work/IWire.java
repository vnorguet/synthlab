package app.work;

import ports.input.ICInput;

public interface IWire {
	public boolean setIn(ICInput destination);
	public void unplugAll();
}

package app.work;

import modules.adsr.CADSR;
import modules.generic.ICModule;
import modules.replicator.CReplicator;
import modules.speaker.CSpeaker;
import modules.vca.CVCA;
import modules.vcf.CVCF;
import modules.vco.CVCO;

public class CWorkPanel implements ICWorkPanel {
	private IPWorkPanel presentation;
	
	public CWorkPanel(){
		presentation = new PWorkPanel();
	}
	
	@Override
	public IPWorkPanel getPresentation() {
		return presentation;
	}

	@Override
	public void addModule(String moduleName) {
		ICModule module = null;
		switch(moduleName){
		case "VCO" : module = new CVCO(); break;
		case "VCA" : module = new CVCA(); break;
		case "VCF" : module = new CVCF(); break;
		case "ADSR" : module = new CADSR(); break;
		case "HP" : module = new CSpeaker(); break;
//		case "File" : module = new CFile(); break;
		case "Replicator" : module = new CReplicator();
		}
		presentation.add(module.getPresentation());
	}

}

package app.work;

import javax.swing.JPanel;

import ports.input.ICInput;
import ports.output.ICOutput;

public class Wire extends JPanel implements IWire {
	private static final long serialVersionUID = 1L;
	private ICOutput out;
	private ICInput in;

/*	private QuadCurve2D courbe;
	private final Color base = new Color(72, 100, 159);
	private final Color highlight = new Color(90, 112, 207);
*/
	public Wire(ICOutput source) {
		out = source;
		out.waitForPlug();
		// setSize(400,400);
		// setPreferredSize(getSize());
		setOpaque(false);
		// courbe = new QuadCurve2D.Float();
	}

	@Override
	public boolean setIn(ICInput destination) {
		boolean res = true;
		if (in == null) {
			in = destination;
			if (!in.plug(out)) {
				res = false;
				in = null;
			}
		}
		return res;
	}
	
/*
	public void paint(Graphics g) {

		super.paint(g);
		if (in != null) {
			Graphics2D g2d = (Graphics2D) g;
			JComponent cIn = (JComponent) in.getPresentation();
			JComponent cOut = (JComponent) out.getPresentation();
			setOpaque(false);
			int minx = Math.min(cIn.getLocationOnScreen().x,
					cOut.getLocationOnScreen().x) - 5;
			int miny = Math.min(cIn.getLocationOnScreen().y,
					cOut.getLocationOnScreen().y) - 5;
			int maxx = Math.max(cIn.getLocationOnScreen().x + cIn.getWidth(),
					cOut.getLocationOnScreen().x + cOut.getWidth()) + 5;
			int maxy = Math.max(cIn.getLocationOnScreen().y + cIn.getHeight(),
					cOut.getLocationOnScreen().y + cOut.getHeight()) + 5;
			setLocation(minx - getParent().getLocationOnScreen().x, miny
					- getParent().getLocationOnScreen().y);

			JComponent outComp = (JComponent) out.getPresentation();
			JComponent inComp = (JComponent) in.getPresentation();
			Point posOut = outComp.getLocationOnScreen();
			Point posIn = inComp.getLocationOnScreen();
			Point start = new Point();
			Point end = new Point();
			start.x = (posOut.x + outComp.getWidth() / 2)
					- getLocationOnScreen().x;
			start.y = (posOut.y + outComp.getHeight() / 2)
					- getLocationOnScreen().y;
			end.x = (posIn.x + inComp.getWidth() / 2) - getLocationOnScreen().x;
			end.y = (posIn.y + inComp.getHeight() / 2)
					- getLocationOnScreen().y;

			courbe.setCurve(start.x, start.y, Math.abs(start.x - end.x) / 2,
					Math.max(start.y, end.y) + 60, end.x, end.y);
			setSize(courbe.getBounds().width + 30,
					courbe.getBounds().height + 20);

			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_OFF); // Paramètre
																// permettant
			// d'accélérer le rendu graphique
			g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_SPEED);
			g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
					RenderingHints.VALUE_COLOR_RENDER_SPEED);
			g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
					RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
			g2d.setRenderingHint(RenderingHints.KEY_DITHERING,
					RenderingHints.VALUE_DITHER_DISABLE);

			g.setColor(Color.GREEN);
			g.fillOval(start.x - 7, start.y - 7, 14, 14);
			g.setColor(Color.YELLOW);
			g.fillOval(end.x - 7, end.y - 7, 14, 14);
			g2d.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND,
					BasicStroke.JOIN_ROUND));
			g.setColor(Color.BLACK); //
			g.drawLine(start.x, start.y, end.x, end.y);
			g2d.draw(courbe);
			g2d.setStroke(new BasicStroke(8, BasicStroke.CAP_ROUND,
					BasicStroke.JOIN_ROUND));
			g.setColor(base);
			g.drawLine(start.x, start.y, end.x, end.y);
			g2d.draw(courbe);
			g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND,
					BasicStroke.JOIN_ROUND));
			g.setColor(highlight);
			g.drawLine(start.x, start.y, end.x, end.y);
			g2d.draw(courbe);
		}
		/*
		 * g.setColor(Color.BLUE); Point begin = ((JPanel)
		 * in.getPresentation()).getLocationOnScreen(); Point end = ((JPanel)
		 * out.getPresentation()).getLocationOnScreen(); g.drawLine(begin.x,
		 * begin.y, end.x, end.y);
		 }
*/

	@Override
	public void unplugAll() {
		if (in != null) {
			in.unplug();
			in = null;
		}
		if (out != null) {
			out.unplug();
			out = null;
		}
	}
}

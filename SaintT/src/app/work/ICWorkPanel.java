package app.work;

public interface ICWorkPanel {

	public IPWorkPanel getPresentation();

	public void addModule(String moduleName);

}

package app.modules;

public interface IPModulePanel {
	public static final String[] MODULE_NAMES = { "VCO", "VCA", "VCF", "ADSR", "Replicator","HP", "File" };

	public ICModulePanel getControl();
}

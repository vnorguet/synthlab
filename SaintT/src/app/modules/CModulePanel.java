package app.modules;

import app.core.ICApp;


public class CModulePanel implements ICModulePanel {

	private ICApp parent;
	private IPModulePanel presentation;
	
	public CModulePanel(ICApp parent){
		this.parent = parent;
		presentation = new PModulePanel(this);
	}

	@Override
	public IPModulePanel getPresentation() {
		return presentation;
	}

	@Override
	public void createModule(String moduleName) {
		parent.addModule(moduleName);
	}
	
	
}

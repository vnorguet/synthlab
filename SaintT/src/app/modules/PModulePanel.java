package app.modules;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import app.actionlisteners.ModuleActionListener;

public class PModulePanel extends JPanel implements IPModulePanel {
	private static final long serialVersionUID = 1L;
	private ICModulePanel control;
	
	public PModulePanel(ICModulePanel control) {
		this.control = control;
		setLayout(new GridLayout(MODULE_NAMES.length,1));
		JButton b = null;
		ActionListener moduleActionListener = new ModuleActionListener(this);
		for(String s : MODULE_NAMES){
			b = new JButton(s);
			add(b);
			b.addActionListener(moduleActionListener);
		}
	}

	@Override
	public ICModulePanel getControl() {
		return control;
	}
}

package app.modules;

public interface ICModulePanel {

	public IPModulePanel getPresentation();

	public void createModule(String moduleName);

}
